﻿using System;
using System.Windows.Input;
using Microsoft.Office.Interop.Word;
using System.Windows.Forms;


namespace DocumentChecker.Model
{

    public class Mistake
    {
        public Mistake()
        {
            GoToErrorCom = new Command(arg => GoToError());
        }

        public Command ShowInfo
        {
            get
            {
                return new Command(arg =>
                {
                    MessageBox.Show(Description);
                });
            }
        }

        public int Position { get; set; }
        public string Text { get; set; }
        public string Description { get; set; }
        public MistakeLevel Level { get; set; }
        public void GoToError()
        {
            try
            {
                object what = WdGoToItem.wdGoToPage;
                object which = WdGoToDirection.wdGoToFirst;
                object count = Position;
                object missing = System.Reflection.Missing.Value;
                WordIOHelper._wordApp.Selection.GoTo(ref what, ref which, ref count, ref missing);
            }
            catch (Exception e)
            {

                MessageBox.Show("Не можна перейти до помилки при режимі тільки для читання або якщо документ закрито");
            }
        }
        public ICommand GoToErrorCom { get; set; }

    }

    public enum MistakeLevel
    {
        Warning,
        Error,
        Information,

    }
}
