﻿using System;
using System.Collections.Generic;

namespace DocumentChecker.Model
{
    public interface IRule
    { 
        /// <summary>
        /// Повернення опису помилок
        /// </summary>
        /// <returns></returns>
        List<Mistake> FindMistakes(DocPart docPart);
        /// <summary>
        /// Деталі помилки
        /// </summary>
        string ErrorDetails { get; }
        string GetDescription();
    }

    [Serializable]
    public abstract class Rule:IRule
    {
        public string RuleName { get; set; }

        public abstract List<Mistake> FindMistakes(DocPart docPart);
        public abstract string ErrorDetails { get; set; }
        public abstract string GetDescription();

        public virtual List<RuleField> GetField()
        {
            return new List<RuleField> { new RuleField("nothing to change", "") };
        }

        public virtual void SetField(List<RuleField> fields)
        {
            
        }
    }
}