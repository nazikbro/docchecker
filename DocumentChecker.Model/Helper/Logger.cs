﻿using System;
using System.Diagnostics;

namespace DocumentChecker.Model.Helper
{
    public static class Logger
    {
        private static TraceSource ts;
        static Logger()
        {
            ts = new TraceSource("myTraceSource");
        }

        public static void Start(string module)
        {
            Info("Method start", module);
        }
        public static void Error(string message, string module)
        {
            WriteEntry(message, "error", module);
        }

        public static void Error(Exception ex, string module)
        {
            WriteEntry(ex.Message, "error", module);
            ts.TraceEvent(TraceEventType.Error, 0, ex.Message);
        }

        public static void Warning(string message, string module)
        {
            WriteEntry(message, "warning", module);
        }

        public static void Info(string message, string module)
        {
            WriteEntry(message, "info", module);
        }

        public static void Info(string message, string module, long execTimeMilisec)
        {
            var elapsedMs = TimeSpan.FromMilliseconds(execTimeMilisec).ToString(@"hh\:mm\:ss\:fff");
            Info(message + "\nDuration: " + elapsedMs, module);
        }

        private static void WriteEntry(string message, string type, string module)
        {
            Trace.WriteLine(
                    string.Format("{0},{1},{2},{3}",
                                  DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                                  type,
                                  module,
                                  message));
        }
    }
}
