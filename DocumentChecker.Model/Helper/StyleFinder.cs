﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Office.Interop.Word;

namespace DocumentChecker.Model.Helper
{
    public class StyleFinder
    {
        public List<Range> headersContents { get; private set; }
        public List<string> headersNames { get; private set; }

        public StyleFinder(Document doc)
        {
            headersNames = new List<string>();
            headersContents = new List<Range>();
            FindHeaders(doc);
        }

        private void FindHeaders(Document doc)
        {
            int position1 = -1;
            int last = 0;
            foreach (Paragraph paragraph in doc.Paragraphs)
            {
                string text = paragraph.Range.Text;

                if (FirstHeadingCheck(paragraph))
                {
                    headersNames.Add(text.Substring(0, text.Length - 1));

                    if (position1 == -1)
                    {
                        position1 = paragraph.Range.End;
                    }
                    else
                    {
                        if (position1 == paragraph.Range.Start)
                        {
                            headersContents.Add(doc.Range(position1, paragraph.Range.Start));
                            position1 = paragraph.Range.End;
                        }
                        else if (position1 < paragraph.Range.End + 1 && paragraph.Range.Start - 1 != 0)
                        {
                            //////////////// /f/t
                            headersContents.Add(doc.Range(position1, paragraph.Range.Start - 1));
                            position1 = paragraph.Range.End;
                        }
                    }
                    //Debug.WriteLine(styleName + " : " + text); 
                }
                last = paragraph.Range.End;
            }
            if (position1 != -1)
                try
                {
                    headersContents.Add(doc.Range(position1, last));
                }
                catch (Exception ex)
                {
                    //пустий розділ   
                    Logger.Error(ex, "StyleFinder");
                }

            else
            {
                headersContents.Add(doc.Range(1, last));
            }
        }

        public static List<Range> FindFonts(Document doc, Font font)
        {
            try
            {
                object missing = System.Reflection.Missing.Value;
                // inRange.Select();

                var changes = new List<Range>();
                doc.TrackRevisions = true;
                var range = doc.Content.Duplicate;
                range.Find.ClearFormatting();
                range.Find.Font = font;

                range.Find.Execute("", missing, missing, missing, missing, missing, true, missing, missing,
                    "Processing...", WdReplace.wdReplaceAll, missing, missing, missing,
                    missing);
                foreach (Revision r in doc.Content.Revisions)
                {
                    if (r.Type != WdRevisionType.wdRevisionInsert)
                    {
                        Range rg = r.Range;
                        if ((rg != null) && (rg.Text != null))
                            changes.Add(rg);
                    }
                    r.Reject();
                }
                doc.TrackRevisions = false;
                return changes;
            }
            catch (Exception e)
            {
                Logger.Error(e, "StyleFinder");
                throw new Exception("Помилка при пошуку шрифта. ");

            }
        }


        public static List<Range> GetHeaderFontsBySize(Document doc)
        {
            List<Range> result = new List<Range>();
            var headersFonts = new List<Font>();
            float maxSize = 0;
            foreach (Style style in doc.Styles)
            {
                Font f = style.Font;
                if (f.Size > 14)
                {
                    headersFonts.Add(f);
                    if (maxSize < f.Size)
                        maxSize = f.Size;
                }
            }

            var maxSizeFonts = headersFonts.Where(f => f.Size == maxSize);

            foreach (var maxSizeFont in maxSizeFonts)
            {
                result.AddRange(FindFonts(doc, maxSizeFont));
            }
            return result;
        }

        /// <summary>
        /// Повернення списку змісту розділів
        /// </summary>
        /// <param name="doc"></param>
        /// <returns></returns>
        public static List<Range> GetHeadersContent(Document doc)
        {
            List<Range> rezult = new List<Range>();

            int position1 = -1;
            int last = 0;
            foreach (Paragraph paragraph in doc.Paragraphs)
            {
                if (FirstHeadingCheck(paragraph))
                {
                    if (position1 == -1)
                    {
                        position1 = paragraph.Range.End;
                    }
                    else
                    {
                        if (position1 == paragraph.Range.Start)
                        {
                            rezult.Add(doc.Range(position1, paragraph.Range.Start));
                            position1 = paragraph.Range.End;
                        }
                        else if (position1 < paragraph.Range.End + 1 && paragraph.Range.Start - 1 != 0)
                        {
                            //////////////// /f/t
                            rezult.Add(doc.Range(position1, paragraph.Range.Start - 1));
                            position1 = paragraph.Range.End;
                        }
                    }
                    //Debug.WriteLine(styleName + " : " + text); 
                }
                last = paragraph.Range.End;
            }
            if (position1 != -1)
                try
                {
                    rezult.Add(doc.Range(position1, last));
                }
                catch (Exception ex)
                {
                    //пустий розділ   
                    Logger.Error(ex, "StyleFinder");
                }

            else
            {
                rezult.Add(doc.Range(1, last));
            }

            return rezult;
        }

        public static List<Range> GetHeadersNames(Document doc)
        {
            List<Range> rezult = new List<Range>();
            try
            {
                foreach (Paragraph paragraph in doc.Paragraphs)
                {

                    if (FirstHeadingCheck(paragraph))
                    {
                        rezult.Add(paragraph.Range);
                    }
                }

            }
            catch (Exception ex)
            {
                {
                    Logger.Error(ex, "StyleFinder");
                }
                throw;
            }

            return rezult;
        }

        /// <summary>
        /// Повернення списку заголовків
        /// </summary>
        /// <param name="doc"></param>
        /// <returns>Список заголовків</returns>
        public static List<string> GetHeadersName(Document doc)
        {
            List<string> rezult = new List<string>();

            foreach (Paragraph paragraph in doc.Paragraphs)
            {
                string text = paragraph.Range.Text;

                if (FirstHeadingCheck(paragraph))
                {
                    rezult.Add(text.Substring(0, text.Length - 1));
                }
            }

            return rezult;
        }

        /// <summary>
        /// Checks if paragraf heading have heading style
        /// </summary>
        /// <param name="paragraph"></param>
        /// <returns></returns>
        private static bool FirstHeadingCheck(Paragraph paragraph)
        {
            return paragraph.OutlineLevel.Equals(WdOutlineLevel.wdOutlineLevel1);
        }
    }
}
