﻿using Microsoft.Office.Interop.Word;
using System;
using System.Runtime.InteropServices;
using DocumentChecker.Model.Helper;

namespace DocumentChecker.Model
{
    public class WordIOHelper
    {
        public static Microsoft.Office.Interop.Word.Application _wordApp { get; set; }

        public WordIOHelper()
        {
            _wordApp = new Microsoft.Office.Interop.Word.Application() { Visible = true };
            // _wordApp.;
        }
        public WordIOHelper(string fileName)
        {
            _wordApp = new Microsoft.Office.Interop.Word.Application() { Visible = true };
            OpenFile(fileName);
        }

        public Document NewFile(string fileName)
        {
            object missing = System.Reflection.Missing.Value;
            return _wordApp.Documents.Add(ref missing, ref missing, ref missing, ref missing);
        }

        public Document OpenFile(string fileName)
        {

            object missingValue = System.Reflection.Missing.Value;
            object readOnlyValue = false;
            object reventValue = true;
            object visibleValue = false;
            try
            {

                _wordApp.Documents.Open(fileName, ref missingValue, readOnlyValue,
                    ref missingValue, ref missingValue,
                    ref missingValue, reventValue,
                    ref missingValue, ref missingValue,
                    ref missingValue, visibleValue,
                    ref missingValue, ref missingValue,
                    ref missingValue, ref missingValue);
            }
            catch (COMException ex)
            {
                Logger.Error(ex,"WordIOHelper");
            }
            return _wordApp.ActiveDocument;
        }

        /*   public static void DisposeInstance()
           {
               _wordApp.DisplayAlerts =WdAlertLevel.wdAlertsNone;
               _wordApp.Documents.Close(null, null, null);
              // _wordApp.Workbooks.Close();
               _wordApp.Quit();
               if (workSheet != null)
                   System.Runtime.InteropServices.Marshal.ReleaseComObject(workSheet);
               if (workBook != null)
                   System.Runtime.InteropServices.Marshal.ReleaseComObject(workBook);
               if (_wordApp != null)
                   System.Runtime.InteropServices.Marshal.ReleaseComObject(_wordApp);
              workSheet = null;
               workBook = null;
               _wordApp = null;
               GC.Collect(); // force final cleanup!
           }*/
    }
}
