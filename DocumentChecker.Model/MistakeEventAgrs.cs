﻿using System;
using System.Collections.Generic;

namespace DocumentChecker.Model
{
    public delegate void FindMistakeEventHandler(object o, MistakeEventAgrs args);
    public class MistakeEventAgrs:EventArgs
    {
        public List<Mistake> TheMistake;
        public MistakeEventAgrs(List<Mistake> inp)
        {
            TheMistake = inp;
        }
       
    }
}
