﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using DocumentChecker.Model.Annotations;

namespace DocumentChecker.Model
{
    public class RuleField:INotifyPropertyChanged
    {
        public string FieldName { get; set; }

        private string _fieldText;
        public string FieldText { 
            get { return _fieldText; }
            set
            {
                _fieldText = value;
                OnPropertyChanged("FieldText");
            } 
        }

        public RuleField(string name, string text)
        {
            FieldName = name;
            FieldText = text;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
