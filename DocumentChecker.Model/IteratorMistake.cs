﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using DocumentChecker.Model.Helper;


namespace DocumentChecker.Model
{
    /// <summary>
    /// Проходження по всіх помилках документу
    /// </summary>
    public class IteratorMistake
    {
        private int _current = 0;
        public IteratorMistake()
        {

        }

        public event FindMistakeEventHandler MistakeFounded;
        public List<Mistake> GetAllMistakes(DocPart doc)
        {
            var result = new List<Mistake>();
            if (doc.Rules != null && doc.Range != null)
                foreach (var rule in doc.Rules)
                {
                    var watch = Stopwatch.StartNew();

                    var ListMistake = rule.FindMistakes(doc);
                    //event invoke
                    if (ListMistake.Count != 0 && MistakeFounded != null)
                    {
                        MistakeFounded(this, new MistakeEventAgrs(ListMistake));
                    }
                    result.AddRange(ListMistake);
                    watch.Stop();
                    Logger.Info($"Execution of {rule.RuleName}", "IteratorMistake", watch.ElapsedMilliseconds);
                }

            if (doc.Parts != null)
                foreach (var _docPart in doc.Parts)
                {
                    var watch = Stopwatch.StartNew();
                    Logger.Info($"Start checking of {_docPart.Caption} chapter", "IteratorMistake");
                    result.AddRange(GetAllMistakes(_docPart));
                    watch.Stop();
                    Logger.Info($"End checking of {_docPart.Caption} chapter", "IteratorMistake", watch.ElapsedMilliseconds);
                }

            return result;
        }
    }
}
