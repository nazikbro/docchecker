﻿using System;

namespace DocumentChecker.Model.Parts
{
    /// <summary>
    /// Розділ документу
    /// </summary>
    [Serializable]
    public class Chapter : TitledDocNode
    {
        public string TitleTemplate { get; set; }
        public ChapterTitleTemplate ChapterTitle { get; set; }
        public bool IsOptional { get; set; }
    }

    /// <summary>
    /// Шаблон заголовку розділу
    /// </summary>
    [Serializable]
    public class ChapterTitleTemplate 
    {
        /// <summary>
        /// Почат розділу
        /// </summary>
        public string StartWith { get; set; }
        public string EqualTo { get; set; }

        public bool IsAviable()
        {
            if((StartWith == null)&&(EqualTo == null))
            {
                return false;
            }            
                return true;            
        }
          
        public bool Match(string name)
        {
            if(EqualTo != null)
            {
                if (name.Equals(EqualTo)) return true;
            }
            if(StartWith != null)
            {
                if (name.Contains(StartWith)) return true;
                if (name.Contains("\f" + StartWith)) return true;
            }
            return false;
        }

    }
}

