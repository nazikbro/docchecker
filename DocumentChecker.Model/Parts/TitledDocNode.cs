﻿using System;
using Microsoft.Office.Interop.Word;

namespace DocumentChecker.Model.Parts
{
    [Serializable]
    public class TitledDocNode:DocPart
    {
        public DocPart Title { get; set; }
    }
}
