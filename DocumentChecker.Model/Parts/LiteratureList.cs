﻿using System;
using DocumentChecker.Model.Parts;
using DocumentChecker.Model.Rules;

namespace Duplom.Model.Parts
{
    [Serializable]
    public class LiteratureList : TitledDocNode
    {
        //Список літератури
        public int LiteratureListElementCount { get; set; }

        public LiteratureList()
        {
            Caption = "Список літературних джерел";
            Rules.Add(new LiteratureListRule());
        }

        public int GetCount()
        {
            return LiteratureListElementCount;
        }
    }
}
