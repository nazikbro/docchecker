﻿using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using DocumentChecker.Model.Annotations;
using Microsoft.Office.Interop.Word;
using System;
using System.Diagnostics;

namespace DocumentChecker.Model.Rules
{
    /// <summary>
    /// Перевірка форматування тексту
    /// </summary>
    [Serializable]
    public class TextFormattingRule : Rule
    {
        public int Size { get; set; }
        public float LineSpacing { get; set; }
        public int Bold { get; set; }
        public int Italic { get; set; }
        public string FontName { get; set; }

        public override List<RuleField> GetField()
        {
            return new List<RuleField>
            {
                new RuleField("Назва шрифту", FontName.ToString()),
                new RuleField("Розмір шрифту", Size.ToString()),
                new RuleField("Міжрядковий інтервал", LineSpacing.ToString()),
                new RuleField("Жирний", Bold.ToString()),
                new RuleField("Курсив", Italic.ToString())
            };
        }

        public override void SetField(List<RuleField> fields)
        {
            List<string> fontNames = new List<string> { "Times New Roman", "Comic Sans" };
            try
            {
                Size = int.Parse(fields[1].FieldText);
                LineSpacing = int.Parse(fields[2].FieldText);
                Bold = int.Parse(fields[3].FieldText);
                Italic = int.Parse(fields[4].FieldText);

                if (fontNames.Contains(fields[0].FieldText))
                    FontName = fields[0].FieldText;
                else MessageBox.Show("Неправильно введена назва шрифту!");
            }
            catch (Exception)
            {
                MessageBox.Show("Виникла помилка! Неправильно заповнені поля!");
            }
        }

        #region Variables
        private bool isSizeFound = false;
        private bool isLineSpacingFound = false;
        private bool isBoldFound = false;
        private bool isItalicFound = false;
        private bool isFontNameFound = false;

        private int SizeECount = 0;
        private int LineSpacingECount = 0;
        private int BoldECount = 0;
        private int ItalicECount = 0;
        private int FontNameECount = 0;
        #endregion
        /// <summary>
        /// Constructor of class
        /// </summary>
        /// <param name="italic">Italic type of font</param>
        /// <param name="lineSpacing">Size of Line Spacing: 12-Normal, 24-Double,18- One & Half</param>
        /// <param name="size">Size of font</param>
        /// <param name="bold">Type of Font-Bold(1-bold,0-not)</param>
        /// <param name="fontName">FontName of the font(like Times New Roman)</param>
        public TextFormattingRule(int size, int bold, int italic, float lineSpacing, string fontName)
        {
            Size = size;
            Bold = bold;
            Italic = italic;
            LineSpacing = lineSpacing;
            FontName = fontName;
        }

        public override List<Mistake> FindMistakes(DocPart docPart)
        {
            var mistakeList = new List<Mistake>();
            const int undefinedValue = -1; // undefined property
            const int maximum = 9999; //maximum value
            //active page number
            var message = GetDescription();
            try
            {
                var page = (int)docPart.Range.Information[WdInformation.wdActiveEndAdjustedPageNumber];
                for (int i = 1; i <= docPart.Range.Paragraphs.Count; i++)
                {
                    var p = docPart.Range.Paragraphs[i];
                    var style = p.get_Style() as Style;
                    var page2 = (int)docPart.Range.Paragraphs[i].Range.Information[WdInformation.wdActiveEndAdjustedPageNumber];
                    string styleName = style.NameLocal;
                    // ReSharper restore PossibleNullReferenceException
                    //  if(p.Range!=word)
                    if (!styleName.Contains("Header") & !styleName.Contains("Заголовок"))
                    {
                        // ReSharper disable CompareOfFloatsByEqualityOperator
                        if (p.Range.Font.Size != Size & p.Range.Font.Size != undefinedValue &
                            p.Range.Font.Size < maximum)
                        // ReSharper restore CompareOfFloatsByEqualityOperator
                        {
                            string errMessage = "Розмір шрифта не відповідає вимогам на " + page2 + " стор в " +
                                                docPart.Caption;
                            mistakeList.Add(new Mistake
                            {
                                Description = message,
                                Level = MistakeLevel.Warning,
                                Position = page2,
                                Text = errMessage
                            });
                        }
                        //Checking the type(Bold,etc.)
                        if (p.Range.Font.Bold != Bold & p.Range.Font.Bold != undefinedValue &
                            p.Range.Font.Bold < maximum)
                        {
                            string errMessage = "Тип шрифта(Півжирний) не відповідає вимогам на " + page2 +
                                                " місцях у " + docPart.Caption;
                            mistakeList.Add(new Mistake
                            {
                                Description = message,
                                Level = MistakeLevel.Warning,
                                Position = page2,
                                Text = errMessage
                            });
                        }
                        //Check style
                        if (p.Range.Font.Name.ToString(CultureInfo.InvariantCulture) != FontName)
                        {
                            string errMessage = "Вид шрифта не відповідає вимогам на " + page2 + " стор в " +
                                                docPart.Caption;
                            mistakeList.Add(new Mistake
                            {
                                Description = message,
                                Level = MistakeLevel.Warning,
                                Position = page2,
                                Text = errMessage
                            });
                        }
                        //Checking type- Italic
                        if (p.Range.Font.Italic != Italic & p.Range.Font.Italic != undefinedValue &
                            p.Range.Font.Italic < maximum)
                        {
                            string errMessage = "Курсив шрифта не відповідає вимогам на " + page2 + " стор в " +
                                                docPart.Caption;
                            mistakeList.Add(new Mistake
                            {
                                Description = message,
                                Level = MistakeLevel.Warning,
                                Position = page2,
                                Text = errMessage
                            });
                        }
                        //Check Line Spacing
                        // ReSharper disable CompareOfFloatsByEqualityOperator
                        if (p.Range.ParagraphFormat.LineSpacing != LineSpacing &
                            p.Range.ParagraphFormat.LineSpacing != undefinedValue)
                        // ReSharper restore CompareOfFloatsByEqualityOperator
                        {
                            string errMessage = "Міжрядковий інтервал не відповідає вимогам на " + page2 +
                                                " стор в " + docPart.Caption;
                            mistakeList.Add(new Mistake
                            {
                                Description = message,
                                Level = MistakeLevel.Warning,
                                Position = page2,
                                Text = errMessage
                            });

                        }
                    }
                }

            }
            catch
            {
                return new List<Mistake>();
            }
            return mistakeList;
        }

        public void MakeMistakeList()
        {

        }
        public override string ErrorDetails { get { return "TextFormattingRule"; } set { } }
        public override string GetDescription()
        {
            const string description = @"Пояснювальна записка повинна бути виконана з чітким шрифтом з контурами символів середньої жирності.";
            return description;
        }
    }
}
