﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Office.Interop.Word;


namespace DocumentChecker.Model.Rules
{
    /// <summary>
    /// Перевіряє правильність підпису таблиці: наявність слова "Таблиця", нумерацію та вирівнювання
    /// </summary>
    [Serializable]
    public class TableCaptionRule : Rule
    {
        public int FontSize { get; set; }
        public int ChapterNum { get; set; }

        public override List<RuleField> GetField()
        {
            return new List<RuleField>
            {
                new RuleField("Розмір шрифта", FontSize.ToString()),
                new RuleField("Номер розділу", ChapterNum.ToString()),
            };
        }

        public override void SetField(List<RuleField> fields)
        {
            try
            {
                FontSize = int.Parse(fields[0].FieldText);
                ChapterNum = int.Parse(fields[1].FieldText);
            }
            catch (Exception)
            {
                MessageBox.Show("Виникла помилка! Неправильно заповнені поля!");
            }
        }

        private Range CaptRange;
        private bool IsTableCaptionNormal(DocPart docPart)
        {
            //  MessageBox.Show(docPart.Document.Tables.Count.ToString());
            for (int i = 1; i <= docPart.Range.Tables.Count; i++)
            {

                CaptRange = docPart.Range.Tables[i].Range;
                CaptRange.End = CaptRange.Start;
                CaptRange.Start -= 90;
                CaptRange.Start = CaptRange.Paragraphs.Last.Range.Start;


                #region dontneed
                /*    for(int j=CaptRange.Start;j<=CaptRange.End;j++)
                   hjej += docPart.Document.Content.Text[j];*/
                //  MessageBox.Show(hjej);
                /*     while (docPart.Document.Content.Text[--CaptRange.Start]!=(char)13)
                     {
                         MessageBox.Show(docPart.Document.Content.Text[CaptRange.Start].ToString()+"   Number of symbol-"+CaptRange.Start.ToString());
                         //     MessageBox.Show(CaptRange.Start.ToString());
                     }*/
                /*   
                 //    MessageBox.Show(docPart.Document.Content.Text[CaptRange.Start - 1].ToString() + docPart.Document.Content.Text[CaptRange.Start].ToString() + docPart.Document.Content.Text[CaptRange.Start + 1].ToString() + "   position =" + CaptRange.Start.ToString());
                    //   MessageBox.Show(docPart.Document.Content.Text[CaptRange.End - 1].ToString() + docPart.Document.Content.Text[CaptRange.End].ToString() + docPart.Document.Content.Text[CaptRange.End + 1].ToString() + "END   position =" + CaptRange.Start.ToString());
            
                          //   MessageBox.Show(CaptRange.Text[CaptRange.Start+1].ToString());
                       //   MessageBox.Show((CaptRange.End - CaptRange.Start).ToString());
                     //     MessageBox.Show(CaptRange.Text);
                 */
                #endregion
                if (!CaptRange.Text.Contains("Таблиця " + ChapterNum + "." + i) || CaptRange.Paragraphs.Last.Alignment != WdParagraphAlignment.wdAlignParagraphRight)
                {
                    return false;

                }

            }
            return true;
        }
        #region IsTableCaptionStyleNormal
        /* private bool IsTableCaptionStyleNormal(DocPart docPart)
         {
             for (int i = 1; i <= docPart.Document.Tables.Count; i++)
             {


                 CaptRange = docPart.Document.Tables[i].Range;
                 CaptRange.End = CaptRange.Start;
                 CaptRange.Start -= 90;
                 CaptRange.Start = CaptRange.Paragraphs.Last.Range.Start;
                 var style = CaptRange.Paragraphs.Last.get_Style() as Style;


                 //--------IF FOR FONT PROPERTY
                 if (style.Font.Size != FontSize )
                 {
                     //     MessageBox.Show("THERE NO NEED FORMAT FOR FONT NEAR TABLE----" + CaptRange.Text.ToString());
                     //     MessageBox.Show("NEMA SLOVA!");
                     return false;

                 }

             }
             return true;
         }*/
#endregion
        public override List<Mistake> FindMistakes(DocPart docPart)
        {
            string descrText = "Назва таблиці повинна складатись з слова 'Таблиця' ,номер таблиці і далі назва таблиці.";
            string descrStyle = "Властивості шрифта, яким написана назва таблиці не відповідають вимогам.(Розмір-14,Вирівнювання - по правому краю)";
            //active page number
            var page = (int)docPart.Range.Information[WdInformation.wdActiveEndAdjustedPageNumber];
            List<Mistake> RetList = new List<Mistake>();
            if (!IsTableCaptionNormal(docPart))
            {
                string err = "Назва таблиці не відповідає вимогам у "+docPart.Caption;
                // MessageBox.Show("THERE IS ERRON WITH YOUR TABLE!!! IN POSITIONS  ---" + CaptRange.Start + "  TO  ---" + CaptRange.End);
                RetList.Add(new Mistake() { Position = page, Level = MistakeLevel.Warning, Text = err, Description = descrText });
            }
            /*if (!IsTableCaptionStyleNormal(docPart))
            {
                string err = "Стиль назви таблиці не відповідає вимогам";
                //     MessageBox.Show("THERE IS ERRON WITH YOUR TABLE!!! IN POSITIONS  ---" + CaptRange.Start + "  TO  ---" + CaptRange.End);
                RetList.Add(new Mistake() { Position = page, Level = MistakeLevel.Warning, Text = err, Description = descrStyle });
            }*/
            return RetList;
        }

        public override string ErrorDetails { get { return "TableCaptionRule"; } set { } }
        public override string GetDescription()
        {
            return null;
        }
    }
}
