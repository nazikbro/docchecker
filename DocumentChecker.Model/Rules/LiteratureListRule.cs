﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Forms;
using DocumentChecker.Model.Annotations;
using Microsoft.Office.Interop.Word;

namespace DocumentChecker.Model.Rules
{
    /// <summary>
    /// Checking literature list rules
    /// </summary>
    [Serializable]
    public class LiteratureListRule : Rule
    {
        public static int Count { get; set; }

        public override List<RuleField> GetField()
        {
            return new List<RuleField>
            {
                new RuleField("К-сть літератури", Count.ToString())
            };
        }

        public override void SetField(List<RuleField> fields)
        {
            try
            {
                Count = int.Parse(fields[0].FieldText);
            }
            catch (Exception)
            {
                MessageBox.Show("Виникла помилка! Неправильно заповнені поля!");
            }
        }

        /// <summary>
        /// Finding all mistakes in Literature List
        /// </summary>
        /// <param name="docPart"> Document</param>
        /// <returns> List of mistakes</returns>'
        public override List<Mistake> FindMistakes(DocPart docPart)
        {
            //Searching for all Literature list mistakes
            var mistakeList = new List<Mistake>();
            try
            {
                if (docPart.Range == null)
                {
                    Count = -1;
                    const string err = "Не виявлено список літератури";
                    return new List<Mistake>
                    {
                        new Mistake
                        {
                            Text = err,
                           // Position = docPart.Range.Start,
                            Level = MistakeLevel.Error,
                            Description = GetDescription()
                        }
                    };
                }
                else
                {
                    for (int i = 1; i <= docPart.Range.ListParagraphs.Count; i++)
                    {
                        Count = docPart.Range.ListParagraphs.Count;
                        var p = docPart.Range.ListParagraphs[i]; //Getting one element of list
                        var range = docPart.Document.Range(p.Range.Start, p.Range.End - 1); //getting range
                        if (!range.Text.Contains("Електронний ресурс") & !range.Text.Contains("Electronic resource"))
                        {
                            mistakeList.AddRange(FindLiteratureListYearMistakes(range));
                            //mistakes in the year of publishing
                            mistakeList.AddRange(FindPageCountExistingMistakes(range)); // page's count mistakes
                            mistakeList.AddRange(FindAuthorInfoMistakes(range)); // author name mistakes
                        }
                    }
                    return mistakeList;
                }
            }
            catch (Exception)
            {
                Count = -1;
                const string err = "Не виявлено список літератури";
                return new List<Mistake>
                {
                    new Mistake
                    {
                        Text =err,Position = docPart.Range.Start,Level = MistakeLevel.Error,Description = GetDescription()
                    }
                };
            }
        }

        ///<summary>
        ///Searching the mistakes in the year of publishing
        ///</summary>
        private IEnumerable<Mistake> FindLiteratureListYearMistakes(Range p)
        {
            var message = GetDescription();
            bool yearFound = false;
            int iyear = 0;
            const String yearDotSeparator = ".";
            String[] words = p.Text.Split(' ');
#pragma warning disable 219
            // int i = 0;
#pragma warning restore 219
            foreach (var word in words)
            {
                if (word.Length < 2) continue;
                if (word.Length == 5) // yyyy+ dot
                {
                    String year = word.Remove(word.Length - 1, 1);
                    bool b = int.TryParse(year, out iyear);
                    if (b & word.Substring(word.Length - 1) == yearDotSeparator)
                    {
                        //Founded Year
                        yearFound = true;
                        iyear = Convert.ToInt32(word.Remove(word.Length - 1, 1));
                    }
                    // i++;
                }

            }
            if (yearFound)
            {
                if (iyear <= 1700 || iyear >= 2100)
                {
                    const string err = "Невірно вказано рік видання";
                    //active page number
                    var page = (int)p.Information[WdInformation.wdActiveEndAdjustedPageNumber];
                    return new List<Mistake>
                    {
                        new Mistake {Description =message, Position = page, Level = MistakeLevel.Warning, Text = err+iyear.ToString(CultureInfo.InvariantCulture)}
                    };
                }
            }
            else
            {
                //Not found the year of publishing
                const string err = "Не виявлено рік видання";
                //active page number
                var page = (int)p.Information[WdInformation.wdActiveEndAdjustedPageNumber];
                return new List<Mistake>
                    {
                        new Mistake {Description = message,Position = page, Level = MistakeLevel.Warning, Text = err}
                    };
            }
            return new List<Mistake>();
        }

        /// <summary>
        /// Check existance of page count
        /// </summary>
        /// <returns>
        /// List of error if Page is not existed
        /// </returns>
        private IEnumerable<Mistake> FindPageCountExistingMistakes(Range p)
        {
            var message = GetDescription();
            const string ukrPageSeparator = "с.";
            const string engPageSeparator = "p.";
            String[] words = p.Text.Split(' ');
            bool flag = false; //is finded?
            int index = -1;
            foreach (var word in words)
            {
                if (word.Length >= 2)
                {
                    if (word.Substring(word.Length - 2, 2) == ukrPageSeparator || word.Substring(word.Length - 2, 2) == engPageSeparator)
                    {
                        int outInt;
                        bool b = int.TryParse(words[index], out outInt);
                        if (b & outInt > 10 & outInt < 2500)//count of page's is in interval [10;2500]
                        {
                            flag = true;
                        }
                        else
                        {
                            const string err = "Невірно задано кількість сторінок";
                            return new List<Mistake>
                                {
                                    new Mistake {Description = message,Position = p.Start, Level = MistakeLevel.Warning, Text = err}
                                };
                        }
                    }
                }

                //
                index++;
            }

            switch (flag)
            {
                case true:
                    return new List<Mistake>();
                default:
                    {
                        //Not found page's count
                        const string err = "Не виявлено кількість сторінок";
                        //active page number
                        var page = (int)p.Information[WdInformation.wdActiveEndAdjustedPageNumber];
                        return new List<Mistake>
                            {
                                new Mistake {Description = message,Position = page, Level = MistakeLevel.Error, Text = err}
                            };
                    }
            }
        }

        /// <summary>
        /// Searching the Author name and surname correction 
        /// </summary>
        private IEnumerable<Mistake> FindAuthorInfoMistakes(Range p)
        {
            //find the second dot -ending of name and surname
            bool authorFound = false;
            var message = GetDescription();
            const string dotSeparator = ".";
            for (int i = 0; i < p.Text.Length - 2; i++)
            {
                if (p.Text[i].ToString(CultureInfo.InvariantCulture) == dotSeparator)
                {
                    var s = p.Text.Substring(0, i);//getting text before dot
                    if (s.Contains(",") //if like aaa bbb, bbb aaa.
                        | p.Text[i + 2].ToString(CultureInfo.InvariantCulture) == dotSeparator) // like Fname N.N.
                    {
                        authorFound = true;
                    }
                }
            }

            switch (authorFound)
            {
                case true:
                    return new List<Mistake>();
                default:
                    {
                        const string err = "Не виявлено автора літератури";
                        //active page number
                        var page = (int)p.Information[WdInformation.wdActiveEndAdjustedPageNumber];
                        return new List<Mistake>
                            {
                                new Mistake {Description =message, Position = page, Level = MistakeLevel.Error, Text = err}
                            };
                    }
            }

        }

        public override string ErrorDetails { get { return "LiteratureListRule"; } set { } }

        public static int GetListCount()
        {
            return Count;

        }
        public override string GetDescription()
        {
            const string description = @"Джерело літератури повинно містити ПІБ автора,
видавництво, рік видання та кількість сторінок, окрім інтернет-джерела";
            return description;
        }
    }
}
