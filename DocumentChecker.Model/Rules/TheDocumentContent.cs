﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using DocumentChecker.Model.Helper;
using DocumentChecker.Model.Parts;
using DocumentChecker.Model.Rules;

namespace DocumentChecker.Model.Docs
{
    [Serializable]
    public class TheDocumentContent : DocPart
    {
        public static Dictionary<string, Type> AllRules = new Dictionary<string, Type>()
        {
            {"Розмір розділу", typeof(ChapterSizeRule)},
            {"Правопис", typeof(CheckSpelling)},
            {"Заголовки", typeof(HeadresStyleRule)},
            {"Список літератури", typeof (LiteratureListRule)},
            {"Розрив сторінки", typeof (PageBrakeRules)},
            {"Нумерування сторінок", typeof (PageNumerationRule)},
            {"Посилання на літературу", typeof (RequiredLiteratureReferenceRule)},
            {"Підписи рисунків", typeof ( ShapeCaptionRule)},
            {"Підписи таблиць", typeof (TableCaptionRule)},
            {"Форматування тексту", typeof (TextFormattingRule)},
            {"Всі вказані розділи", typeof (ContentConformityRule)},
        };

        public void LoadDoc(string filePath, string docXml)
        {
            if (!System.IO.File.Exists(filePath)) return;

            var ioHelper = new WordIOHelper();
            Document = ioHelper.OpenFile(filePath);
            //   Parser.Parse(Document);
            Range = Document.Content;
            LoadRules(docXml);

            foreach (var part in Parts)
            {
                part.Document = Document;
            }

            MakeChapters();
        }

        protected void MakeChapters()
        {
        
            Stopwatch watch;
            Logger.Info("\nStart of getting headers", "TheDocumentContent");
            watch = Stopwatch.StartNew();
            var headers = new StyleFinder(Document);
            watch.Stop();
            Logger.Info("Ends of getting headers", "TheDocumentContent", watch.ElapsedMilliseconds);

            var headersInDoc = headers.headersNames;
            var docRanges = headers.headersContents;
            var chaptersInDoc = new List<Chapter>();

            Logger.Start("TheDocumentContent");
            Logger.Info("\nStart of chapters searching", "TheDocumentContent");
            watch = Stopwatch.StartNew();
            foreach (var part in Parts)
            {
                if (part.GetType() == typeof(Chapter))
                {
                    if ((part as Chapter).ChapterTitle.IsAviable())
                    {
                        chaptersInDoc.Add((part as Chapter));
                    }
                }
            }

            for (int i = 0; i < headersInDoc.Count; i++)
            {
                for (int j = 0; j < chaptersInDoc.Count; j++)
                {
                    if (chaptersInDoc[j].ChapterTitle.Match(headersInDoc[i]))
                    {
                        chaptersInDoc[j].Range = docRanges[i];
                        Logger.Info($"Found of {chaptersInDoc[j].ChapterTitle} chapter", "TheDocumentContent");
                        break;
                    }
                }
            }
            watch.Stop();
            Logger.Info("Ends of chapter searching", "TheDocumentContent", watch.ElapsedMilliseconds);
        }
    }
}
