﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Office.Interop.Word;

namespace DocumentChecker.Model.Rules
{
    /// <summary>
    /// Пошук великої кількості рпобілів в кінці кожного розділу
    /// Застосовується лиш для окремих розділів
    /// </summary>
    [Serializable]
    public class PageBrakeRules : Rule
    {

        private Range CaptRange;
        private bool IsPageBrakeNormal(DocPart docPart)
        {
            int CountOfEnters = 0;
            int tempLen = 0;

            for (int i = docPart.Document.Content.Text.Length - 1; i >= docPart.Document.Content.Text.Length - 10; i--)
            {

                if (CountOfEnters > 4) break;

                if (docPart.Document.Content.Text[i] == (char)13)
                {
                    CountOfEnters += 1;
                }
            }
            if (CountOfEnters > 4)
                return true;
            else return false;
        }

        public override List<Mistake> FindMistakes(DocPart docPart)
        {
            List<Mistake> RetList = new List<Mistake>();
            //active page number
            var page = (int)docPart.Range.Information[WdInformation.wdActiveEndPageNumber];
            if (IsPageBrakeNormal(docPart))
            {
                string err = "Знайдено багато ентерів замість використання розриву стоорінки перед" +docPart.Caption;
                RetList.Add(new Mistake() {Position = page, Level = MistakeLevel.Information, Text = err, Description = "Для переходу до наступної сторінки при недостатньому обсязі матеріалів на поточній варто викорстовувати розрив сторінки (Ctrl+Enter)" });
            }
            return RetList;
        }

        public override string ErrorDetails { get { return "PageBrakeRules"; } set { } }
        public override string GetDescription()
        {
            throw new NotImplementedException();
        }
    }
}
