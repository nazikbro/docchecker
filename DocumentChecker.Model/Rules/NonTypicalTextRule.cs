﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Word;

namespace DocumentChecker.Model.Rules
{
    /// <summary>
    /// Перевірка чи розділ названо нетипово
    /// </summary>
    [Serializable]
    public class NonTypicalTextRule : TypicalTextRule
    {
        public NonTypicalTextRule(List<string> typicalNames) : base(typicalNames)
        {
            

        }

        public override List<Mistake> FindMistakes(DocPart docPart)
        {
            if (!AreAllWordsTypical(docPart))
            {
     //           WordIOHelper.DisposeInstance();
                return new List<Mistake>();
            }
            else
            {
                //active page number
                var page = (int)docPart.Range.Information[WdInformation.wdActiveEndAdjustedPageNumber];
                var err = string.Format("Некорректна назва для: {0}. Цей текст не може бути стандартний.", docPart.Caption);
                return new List<Mistake>()
                           {new Mistake() {Position = page, Level = MistakeLevel.Error, Text = err}};
            }
        }

        public override string ErrorDetails
        {
            get { return "NonTypicalTextRule"; }
            set { }
        }
    }
}
