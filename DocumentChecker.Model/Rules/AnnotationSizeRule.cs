﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DocumentChecker.Model.Helper;
using DocumentChecker.Model.Parts;
using Microsoft.Office.Interop.Word;

namespace DocumentChecker.Model.Rules
{
    /// <summary>
    /// Правило перевірки розміру анотацій
    /// </summary>
    [Serializable]
    public class AnnotationSizeRule : Rule
    {
        public override List<Mistake> FindMistakes(DocPart docPart)
        {
            var rezult = new List<Mistake>();
            //active page number
            var page = (int)docPart.Range.Information[WdInformation.wdActiveEndAdjustedPageNumber];
            var headers = StyleFinder.GetHeadersNames(docPart.Document);

            try
            {
                int ukrPageStart = headers[0].get_Information(WdInformation.wdActiveEndPageNumber);
                int engPageStart = headers[1].get_Information(WdInformation.wdActiveEndPageNumber);

                int ukrPageEnd = docPart.Parts[0].Range.get_Information(WdInformation.wdActiveEndPageNumber);
                int engPageEnd = docPart.Parts[1].Range.get_Information(WdInformation.wdActiveEndPageNumber);

                if (ukrPageEnd - ukrPageStart != 0)
                    rezult.Add(new Mistake()
                                   {
                                       Description = GetDescription(),
                                       Level = MistakeLevel.Error,
                                       Text =
                                           string.Format("Анотація українською займає {0} сторінки",
                                                         ukrPageEnd - ukrPageStart + 1),
                                       Position = page
                                   });
                if (engPageEnd - engPageStart != 0)
                    rezult.Add(new Mistake()
                    {
                        Description = GetDescription(),
                                       Level = MistakeLevel.Error,
                                       Text =
                                           string.Format("Анотація англійською займає {0} сторінки",
                                                         engPageEnd - engPageStart + 1),
                                       Position = page
                                   });
            }
            catch (Exception)
            {
                rezult.Add(new Mistake()
                {
                    Description = "Документ повинен містити анотацію",
                    Level = MistakeLevel.Error,
                    Position = page,
                    Text = string.Format("В документі не знайдено анотації")
                });
            }
            return rezult;
        }

        public override string ErrorDetails { get { return "AnnotationSizeRule"; }
            set { }
        }
        public override string GetDescription()
        {
            return "Анотація повинна займати одну сторінку";
        }
    }
}
