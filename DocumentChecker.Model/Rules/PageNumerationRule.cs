﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Office.Interop.Word;

namespace DocumentChecker.Model.Rules
{
    /// <summary>
    /// Перевірка існування та правильності нумерування сторінок
    /// </summary>
    [Serializable]
    public class PageNumerationRule : Rule
    {
        private bool Passed = true;

        public bool CheckPageNumAlightment(DocPart docPart)
        {
            var Alignment = docPart.Document.Sections[1].Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.Paragraphs[1].Alignment;
            var page = (int)docPart.Range.Information[WdInformation.wdActiveEndAdjustedPageNumber];
            return Alignment == WdParagraphAlignment.wdAlignParagraphRight
                ? false
                : true;
        }

        public bool CheckFirstPageNum(DocPart docPart)
        {
            var page = (int)docPart.Range.Information[WdInformation.wdActiveEndAdjustedPageNumber];
            return docPart.Document.Sections[1].Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].PageNumbers.
                ShowFirstPageNumber == true ? true : false;
        }

        /*public Mistake CheckPageNum(DocPart docPart)
        {
           var numberOfPages = docPart.Document.ComputeStatistics(wds);
                var RestartNumberingAtSection = docPart.Document.Sections[1].Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].PageNumbers.RestartNumberingAtSection;//.
                var StartingNumber = docPart.Document.Sections[1].Headers[WdHeaderFooterIndex.wdHeaderFooterFirstPage].PageNumbers.
                    StartingNumber;
              
            if (StartingNumber != 2 && RestartNumberingAtSection == true)
                ret_list.Add(new Mistake()
                {
                    Level = MistakeLevel.Error,
                    Position = page,
                    Text = "Помилка в нумерації сторінок (має починатись з 2)",
                    Description = "Нумерація сторінок не має відображатись на першій сторінці або неправильний початковий номер"
                });
        }*/

        public bool isPageNum(DocPart docPart)
        {
            var snum = docPart.Document.Sections[1].Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range.Text[0].ToString();
            var page = (int)docPart.Range.Information[WdInformation.wdActiveEndAdjustedPageNumber];
            int outInt;
            bool b = int.TryParse(snum, out outInt);
            if (b && outInt > 0)
            {
                return false;
            }
            else
            {
                Passed = false;
                return true;
            }
        }

        public override List<Mistake> FindMistakes(DocPart docPart)
        {
            //active page number
            var page = (int)docPart.Range.Information[WdInformation.wdActiveEndAdjustedPageNumber];
            var ret_list = new List<Mistake>();
            //ret_list.Add(isPageNum(docPart));
            if (isPageNum(docPart))
            {
                ret_list.Add(new Mistake()
                {
                    Level = MistakeLevel.Error,
                    Position = page,
                    Text = "Немає нумерації сторінок",
                    Description = "Колонтитулі повинна бути лишень нумерація сторінок"
                });

            }
            if (Passed)
            {

                if (CheckPageNumAlightment(docPart))
                {
                    ret_list.Add(new Mistake()
                    {
                        Level = MistakeLevel.Error,
                        Position = page,
                        Text = "Помилка в вирівнюванні нумерації сторінок",
                        Description = "Вирівнювання нумерації сторінок має бути по правому краї"
                    });
                }
               /* if (CheckFirstPageNum(docPart))
                {
                    ret_list.Add(new Mistake()
                    {
                        Level = MistakeLevel.Warning,
                        Position = page,
                        Text = "Номер сторінки не має відображатись на титульній сторінці",
                        Description = "Номер сторінки не має відображатись на титульній сторінці"
                    });
                }*/
                
            }
            return ret_list;
        }

        public override string ErrorDetails { get { return "PageNumerationRule"; } set { } }
        public override string GetDescription()
        {
            throw new NotImplementedException();
        }
    }
}
