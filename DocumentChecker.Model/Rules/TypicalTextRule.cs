﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Word;

namespace DocumentChecker.Model.Rules
{
    /// <summary>
    /// Перевірка чи розділ названо типово
    /// </summary>
    [Serializable]
    public class TypicalTextRule : Rule
    {

        private List<string> _typicalWords = new List<string>();

        public TypicalTextRule(List<string> _typicalWords)
        {
            this._typicalWords = _typicalWords;
        }

        public override List<Mistake> FindMistakes(DocPart docPart)
        {
            if (AreAllWordsTypical(docPart))
            {
                return new List<Mistake>();
            }
            else
            {
                //active page number
                var page = (int)docPart.Range.Information[WdInformation.wdActiveEndAdjustedPageNumber];
                var err = string.Format("Нетипова назва для: " + tempStr);
                return new List<Mistake>() { new Mistake() { Position = page, Level = MistakeLevel.Warning, Text = err } };
            }
        }

        protected string TypycalWordsToList()
        {
            StringBuilder b = new StringBuilder();
            foreach (var typicalWord in _typicalWords)
            {
                b.Append(typicalWord);
                b.Append(", ");
            }
            b.Remove(b.Length - 2, 2);
            return b.ToString();
        }

        public override string ErrorDetails
        {
            get
            {
                return "TypicalTextRule Назва повинна складатись з слів: " + TypycalWordsToList();
            }
            set { }
        }

        public override string GetDescription()
        {
            throw new NotImplementedException();
        }

        private string tempStr = null;
        protected bool AreAllWordsTypical(DocPart docPart)
        {
            if (docPart.Range != null)
                foreach (Range word in docPart.Range.Words)
                {
                    if (word.Text != "\r")
                        return true;
                    if (!_typicalWords.Contains(word.Text))
                    {
                        tempStr = word.Text;
                        return false;
                    }
                }
            return true;
        }
    }
}
