﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Word;

namespace DocumentChecker.Model.Rules
{
    /// <summary>
    /// Правило перевірки і підрахунку помилок правопису
    /// </summary>
    [Serializable]
    public class CheckSpelling : Rule
    {
        public override List<Mistake> FindMistakes(DocPart docPart)
        {
            // Microsoft.Office.Interop.Word.SpellingSuggestions listOfSuggestions =docPart.Document.Content.GetSpellingSuggestions();

            //listOfSuggestions[0].Name;


            //TO FIX SPELLING ERRORS NEED RUN THIS--->>>>      //docPart.Document.Content.CheckSpelling();


            int Count_of_errors = docPart.Document.Content.SpellingErrors.Count;


            List<Mistake> RetList = new List<Mistake>();
            //active page number
            var page = (int)docPart.Range.Information[WdInformation.wdActiveEndAdjustedPageNumber];
            if (Count_of_errors >= 1)
            {
                string err = "Перевірте будь ласка правопис слів у документі. Там " + Count_of_errors + " помилок!";
                // MessageBox.Show("THERE IS ERRON WITH YOUR TABLE!!! IN POSITIONS  ---" + CaptRange.Start + "  TO  ---" + CaptRange.End);
                RetList.Add(new Mistake() { Position = page, Level = MistakeLevel.Information, Text = err, Description = "Для виправлення помилок використайте перевірку правопису слів." });
            }
            return RetList;
        }

        public override string ErrorDetails
        {
            get
            {
                return "CheckSpelling";
            }
            set { }
        }
        public override string GetDescription()
        {
            throw new NotImplementedException();
        }
    }
}
