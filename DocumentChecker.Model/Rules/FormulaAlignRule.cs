﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Word;

namespace DocumentChecker.Model.Rules
{
    /// <summary>
    /// Правило перевірки розміщення формул
    /// </summary>
    [Serializable]
    public class FormulaAlignRule : Rule
    {
        public override List<Mistake> FindMistakes(DocPart docPart)
        {
            //active page number
            var page = (int)docPart.Range.Information[WdInformation.wdActiveEndAdjustedPageNumber];
            var ret = new List<Mistake>();
            foreach (OMath t in docPart.Range.OMaths)
            {
                
                if (t.Justification != WdOMathJc.wdOMathJcRight)
                    ret.Add(new Mistake(){Text = "Помилка у вирівнюванні формули",Level = MistakeLevel.Warning,Position =page, Description = "Математичні формули мають бути вирівняні по правому краю."});
            }
            return ret;
        }


        public override string ErrorDetails { get { return "FormulaAlignRule"; } set { } }
        public override string GetDescription()
        {
            return "Вирівнювання математичних формул має бути по правому краю";
        }
    }
}
