﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Office.Interop.Word;

namespace DocumentChecker.Model.Rules
{
    /// <summary>
    /// Перевіряє правильність підпису рисунку: наявність слова "Рис", нумерацію та вирівнювання
    /// </summary>
    [Serializable]
    public class ShapeCaptionRule : Rule
    {
        public int FontSize { get; set; }
        public int ChapterNum { get; set; }

        public override List<RuleField> GetField()
        {
            return new List<RuleField>
            {
                new RuleField("Розмір шрифта", FontSize.ToString()),
                new RuleField("Номер розділу", ChapterNum.ToString()),
            };
        }

        public override void SetField(List<RuleField> fields)
        {
            try
            {
                FontSize = int.Parse(fields[0].FieldText);
                ChapterNum = int.Parse(fields[1].FieldText);
            }
            catch (Exception)
            {
                MessageBox.Show("Виникла помилка! Неправильно заповнені поля!");
            }
        }

        private Range CaptRange;
        private bool IsShapeCaptionNormal(DocPart docPart)
        {

            for (int i = 1; i <= docPart.Range.InlineShapes.Count; i++)
            {

                CaptRange = docPart.Range.InlineShapes[i].Range;
                CaptRange.Start = CaptRange.End + 1;
                CaptRange.End += 90;
                CaptRange.End = CaptRange.Paragraphs.First.Range.End;
                String[] words = CaptRange.Text.Split(' ', '.');
              /*  if (words[0] == "\r\a")
                {
                    return true;
                }*/
                if (words[0] != "Рис")
                {
                    return false;
                }
                if (words[1] != ChapterNum.ToString())
                {
                    return false;
                }
                if (words[2] != i.ToString() )
                {
                    return false;
                }
                if (CaptRange.ParagraphFormat.Alignment != WdParagraphAlignment.wdAlignParagraphCenter)
                {
                    return false;
                }
            }
            return true;
        }
        #region IsShapeCaptionStyleNormal
        /* 
        private bool IsShapeCaptionStyleNormal(DocPart docPart)
        {
            for (int i = 1; i <= docPart.Document.InlineShapes.Count; i++)
            {
                CaptRange = docPart.Document.InlineShapes[i].Range;
                CaptRange.Start = CaptRange.End + 1;
                CaptRange.End += 90;
                CaptRange.End = CaptRange.Paragraphs.First.Range.End;
                var style = CaptRange.get_Style() as Style;


                //--------IF FOR FONT PROPERTY
                if (style.Font.Size != FontSize)
                {
                //    MessageBox.Show("THERE NO NEED FORMAT FOR FONT NEAR TABLE----" + CaptRange.Text.ToString());
                    //     MessageBox.Show("NEMA SLOVA!");
                    return false;

                }

            }
            return true;
        }*/
        #endregion

        public override List<Mistake> FindMistakes(DocPart docPart)
        {
            string descrText = "Назва рисунку повинна складатись з слова 'Рисунок' ,номер рисунку і далі назва рисунку.";
            string descrStyle = "Властивості шрифта, яким написана назва рисунку не відповідають вимогам.(Розмір-14,Вирівнювання - по центру)";
            //active page number
            var page = (int)docPart.Range.Information[WdInformation.wdActiveEndAdjustedPageNumber];
            List<Mistake> RetList = new List<Mistake>();
            if (!IsShapeCaptionNormal(docPart))
            {
                string err = "Назва рисунку не відповідає вимогам у " + docPart.Caption;
                //            MessageBox.Show("THERE IS ERRON WITH YOUR PICTURE!!! IN POSITION ---" + CaptRange.Start + "  TO  ---" + CaptRange.End);
                RetList.Add(new Mistake() { Position = page, Level = MistakeLevel.Error, Text = err, Description = descrText });
            }
            /*  if (!IsShapeCaptionStyleNormal(docPart))
              {
                  string err = "Стиль назви рисунку не відповідає вимогам";
                //  MessageBox.Show("THERE IS ERRON WITH YOUR Picture!!! IN POSITIONS  ---" + CaptRange.Start + "  TO  ---" + CaptRange.End);
                  RetList.Add(new Mistake() { Position = page, Level = MistakeLevel.Error, Text = err, Description = descrStyle});
              }*/
            return RetList;
        }

        public override string ErrorDetails { get { return "ShapeCaptionRule"; } set { } }
        public override string GetDescription()
        {
            throw new NotImplementedException();
        }
    }
}
