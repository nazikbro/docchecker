﻿using System.Collections.Generic;
using System.Globalization;
using DocumentChecker.Model.Annotations;
using Microsoft.Office.Interop.Word;
using System;
using System.Diagnostics;

namespace DocumentChecker.Model.Rules
{
    /// <summary>
    /// Перевірка форматування тексту
    /// </summary>
    [Serializable]
    class TextFormattingRule2 : Rule
    {
        public int Size { get; set; }
        public float LineSpacing { get; set; }
        public int Bold { get; set; }
        public int Italic { get; set; }
        public string FontName { get; set; }

        public override List<RuleField> GetField()
        {
            return new List<RuleField>
            {
                new RuleField("Font name", FontName.ToString()),
                new RuleField("Font size", Size.ToString()),
                new RuleField("Line spacing", LineSpacing.ToString()),
                new RuleField("Is bold", Bold.ToString()),
                new RuleField("Line spacing", Italic.ToString())
            };
        }

        #region Variables
        private bool isSizeFound = false;
        private bool isLineSpacingFound = false;
        private bool isBoldFound = false;
        private bool isItalicFound = false;
        private bool isFontNameFound = false;

        private int SizeECount = 0;
        private int LineSpacingECount = 0;
        private int BoldECount = 0;
        private int ItalicECount = 0;
        private int FontNameECount = 0;
        #endregion
        /// <summary>
        /// Constructor of class
        /// </summary>
        /// <param name="italic">Italic type of font</param>
        /// <param name="lineSpacing">Size of Line Spacing: 12-Normal, 24-Double,18- One & Half</param>
        /// <param name="size">Size of font</param>
        /// <param name="bold">Type of Font-Bold(1-bold,0-not)</param>
        /// <param name="fontName">FontName of the font(like Times New Roman)</param>
        public TextFormattingRule2(int size, int bold, int italic, float lineSpacing, string fontName)
        {
            Size = size;
            Bold = bold;
            Italic = italic;
            LineSpacing = lineSpacing;
            FontName = fontName;
        }

        public override List<Mistake> FindMistakes(DocPart docPart)
        {
            var mistakeList = new List<Mistake>();
            const int undefinedValue = -1; // undefined property
            const int maximum = 9999; //maximum value
            //active page number
            var message = GetDescription();
            try
            {
                var page = (int)docPart.Range.Information[WdInformation.wdActiveEndAdjustedPageNumber];
            
                for (int i = 1; i <= docPart.Range.Paragraphs.Count; i++)
                {                 
                    var p = docPart.Range.Paragraphs[i];           
                    var style = p.get_Style() as Style;
                    string styleName = style.NameLocal;
                    // ReSharper restore PossibleNullReferenceException
                    if (!styleName.Contains("Header") & !styleName.Contains("Заголовок"))
                    {

                        if (p.Range.Font.Size != Size & p.Range.Font.Size != undefinedValue & p.Range.Font.Size < maximum)

                        {
                            isSizeFound = true;
                            SizeECount++;
                        }
                        //Checking the type(Bold,etc.)
                        if (p.Range.Font.Bold != Bold & p.Range.Font.Bold != undefinedValue & p.Range.Font.Bold < maximum)
                        {
                            isBoldFound = true;
                            BoldECount++;
                        }
                        //Check style
                        if (p.Range.Font.Name.ToString(CultureInfo.InvariantCulture) != FontName)
                        {
                            isFontNameFound = true;
                            FontNameECount++;
                        }
                        //Checking type- Italic
                        if (p.Range.Font.Italic != Italic & p.Range.Font.Italic != undefinedValue & p.Range.Font.Italic < maximum)
                        {
                            isItalicFound = true;
                            ItalicECount++;
                        }
                        //Check Line Spacing
                        // ReSharper disable CompareOfFloatsByEqualityOperator
                        if (p.Range.ParagraphFormat.LineSpacing != LineSpacing &
                            p.Range.ParagraphFormat.LineSpacing != undefinedValue)
                        // ReSharper restore CompareOfFloatsByEqualityOperator
                        {
                            isLineSpacingFound = true;
                            LineSpacingECount++;
                        }
                    }
                }
                if (isSizeFound)
                {
                    string errMessage = "Розмір шрифта не відповідає вимогам у " + SizeECount + " місцях у " + docPart.Caption;
                    mistakeList.Add(new Mistake
                    {
                        Description = message,
                        Level = MistakeLevel.Warning,
                        Position = page,
                        Text = errMessage
                    });
                }
                if (isBoldFound)
                {
                    string errMessage = "Тип шрифта(Півжирний) не відповідає вимогам у " + BoldECount + " місцях у " + docPart.Caption;
                    mistakeList.Add(new Mistake
                    {
                        Description = message,
                        Level = MistakeLevel.Warning,
                        Position = page,
                        Text = errMessage
                    });

                }
                if (isFontNameFound)
                {
                    string errMessage = "Вид шрифта не відповідає вимогам у " + FontNameECount + " місцях у " + docPart.Caption;
                    mistakeList.Add(new Mistake
                    {
                        Description = message,
                        Level = MistakeLevel.Warning,
                        Position = page,
                        Text = errMessage
                    });

                }
                if (isItalicFound)
                {
                    string errMessage = "Курсив шрифта не відповідає вимогам у " + ItalicECount + " місцях у " + docPart.Caption;
                    mistakeList.Add(new Mistake
                    {
                        Description = message,
                        Level = MistakeLevel.Warning,
                        Position = page,
                        Text = errMessage
                    });

                }
                if (isLineSpacingFound)
                {
                    string errMessage = "Міжрядковий інтервал не відповідає вимогам у " + LineSpacingECount + " місцях у" + docPart.Caption;
                    mistakeList.Add(new Mistake
                    {
                        Description = message,
                        Level = MistakeLevel.Warning,
                        Position = page,
                        Text = errMessage
                    });

                }
            }
            catch
            {
                return new List<Mistake>();
            }
            return mistakeList;
        }

        public override string ErrorDetails { get { return "TextFormattingRule"; }
            set { }
        }
        public override string GetDescription()
        {
            const string description = @"Пояснювальна записка повинна бути виконана з чітким шрифтом з контурами символів середньої жирності.";
            return description;
        }
    }
}
