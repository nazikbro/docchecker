﻿using System.Collections.Generic;
using System.Globalization;
using DocumentChecker.Model.Annotations;
using Microsoft.Office.Interop.Word;
using System;

namespace DocumentChecker.Model.Rules
{
    /// <summary>
    /// Перевірка непустих посилань на літературу
    /// </summary>
    [Serializable]
    public class RequiredLiteratureReferenceRule : Rule
    {
        public override List<Mistake> FindMistakes(DocPart docPart)
        {
            //Find '[' and ']'
           
            const string leftSymbol = "[";
            const string rightSymbol = "]";
            bool leftFound = false;
            int leftIndex = -1;
            var mistakeList = new List<Mistake>();
            try
            {
                for (int i = 0; i < docPart.Range.Text.Length; i++)
                {
                    if (docPart.Range.Text[i].ToString(CultureInfo.InvariantCulture) == leftSymbol)
                    {
//found [
                        leftFound = true;
                        leftIndex = i + 1;
                    }
                    if (docPart.Range.Text[i].ToString(CultureInfo.InvariantCulture) == rightSymbol & leftFound)
                    {
                        //found ]
                        int rightIndex = i - 1;
                        string insideString = docPart.Range.Text.Substring(leftIndex, rightIndex - leftIndex + 1);
                        char[] sep = {' ', '-', '–',',', 'c', 'с', '.'};
                        string[] strings = insideString.Split(sep,StringSplitOptions.RemoveEmptyEntries);
                        foreach (var s in strings)
                        {
                            int outInt;
                            bool foundReference = int.TryParse(s, out outInt);
                            if (!foundReference)
                            {
                                //active page number
                                var page = (int)docPart.Range.Information[WdInformation.wdActiveEndAdjustedPageNumber];
                                const string err = "В описовому розділі обовязково повинні бути посилання на літературу";
                                var mistake = new Mistake
                                    {
                                        Description = GetDescription(),
                                        Text = err,
                                        Position = page,
                                        Level = MistakeLevel.Warning
                                    };
                                mistakeList.Add(mistake);
                            }
                        }
                    }
                }
            }
// ReSharper disable EmptyGeneralCatchClause
            catch
// ReSharper restore EmptyGeneralCatchClause
            {
                
            }
            return mistakeList;
        }

        public override string ErrorDetails { get { return "RequiredLiteratureReferenceRule"; } set { } }
        public override string GetDescription()
        {
            return "Описовий розділ обовязково повинен містити посилання на джерело літератури.";
        }
    }
}
