﻿using System;
using System.Collections.Generic;
using System.Globalization;
using DocumentChecker.Model.Annotations;
using Microsoft.Office.Interop.Word;
using System.Diagnostics;

namespace DocumentChecker.Model.Rules
{
    /// <summary>
    /// Class for Additionals (додатки)
    /// </summary>
  [Serializable]
  public  class AdditionalsRule:Rule
    {
        public override List<Mistake> FindMistakes(DocPart docPart)
        {
            const string addiSymbol = "Додаток";
            int addiIndex = 0;
            string[] correctSymbols = { "А", "Б", "В", "Д", "Е", "И", "Ж", "З", "К", "Л", "М"};
            
            for (int i = 0; i <docPart.Range.Text.Length-addiSymbol.Length-2; i++)
            {
                if (docPart.Range.Text.Substring(i,addiSymbol.Length)==addiSymbol)//Found Additionals Symbol
                {
                    //
                    string A=docPart.Range.Text[i + addiSymbol.Length + 1].ToString(CultureInfo.InvariantCulture);
                    string B = correctSymbols[addiIndex];
                   
                    if (A.Equals(B))
                    {
                        //All is OKAY!
                        addiIndex++;
                    }
                    else
                    {//not correct
                        const string err = "Неправильно вказана нумерація додатків";
                        var message = GetDescription();
                        //active page number
                        var page=(int)docPart.Range.Information[WdInformation.wdActiveEndAdjustedPageNumber];
                        return new List<Mistake> { new Mistake {Description = message,Level = MistakeLevel.Error, Position = page, Text = err } };
                    } 
                }
                
            }
            //Found additionals

            return new List<Mistake>();
        }

        public override string ErrorDetails { get { return "AdditionalsRule"; }
            set { }
        }
        public override string GetDescription()
        {
            const string description = @"Додатки слід позначати послідовно великими літерами " +
                                       "української абетки, за винятком Г,Є,І,Ї,Й,О,Ч,Ь. Один додаток" +
                                       "позначається як \"Додаток А\"";
            return description;
        }
    }
}
