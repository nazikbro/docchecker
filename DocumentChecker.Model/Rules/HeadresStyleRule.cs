﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Word;

namespace DocumentChecker.Model.Rules
{
    [Serializable]
    public class HeadresStyleRule : Rule
    {
        public override List<Mistake> FindMistakes(DocPart docPart)
        {
            var rezult = new List<Mistake>();
            //active page number
            var page = (int)docPart.Range.Information[WdInformation.wdActiveEndAdjustedPageNumber];
            //
            foreach (Paragraph paragraph in docPart.Document.Paragraphs)
            {
                Style style = paragraph.get_Style() as Style;
                string styleName = style.NameLocal;
                var a = style.ParagraphFormat.Alignment;
                if ((styleName == "Заголовок 1") || (styleName == "Heading 1"))
                {
                    if (paragraph.Alignment != WdParagraphAlignment.wdAlignParagraphCenter)
                    {
                        rezult.Add(new Mistake()
                        {
                            Description = GetDescription(),
                            Level = MistakeLevel.Error,
                            Text = string.Format("Заголовок '{0}' не відповідає правилам форматування", 
                                    paragraph.Range.Text.Substring(0, paragraph.Range.Text.Length - 1)),
                            Position = page
                        }
                        );
                    }
                }
            }
            //
            return rezult;
        }

        public override string ErrorDetails { get { return "HeadresStyleRule"; } set { } }

        public override string GetDescription()
        {
            return "Усі заголовки повинні мати стиль 'Заголовок 1', розміщені по лівому краю листа";
        }
    }
}
