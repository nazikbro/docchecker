﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DocumentChecker.Model.Helper;
using DocumentChecker.Model.Parts;
using Microsoft.Office.Interop.Word;

namespace DocumentChecker.Model.Rules
{
    /// <summary>
    /// Перевірка змісту і правильного розташування розділів
    /// </summary>
    [Serializable]
    public class ContentConformityRule : Rule
    {

        public override List<Mistake> FindMistakes(DocPart docPart)
        {
            List<Mistake> result = new List<Mistake>();
            if (docPart.Parts == null) return result;

            for (int i = 0; i < docPart.Parts.Count; i++)
            {
                var part = docPart.Parts[i];
                if (part.Range == null)
                {
                    result.Add(new Mistake()
                    {
                        Description = GetDescription(),
                        Position = 3,
                        Level = MistakeLevel.Error,
                        Text = $"Не знайдено розділ: {part.Caption}"
                    });
                }
                else if (i > 0 && (part.Range.Start < docPart.Parts[i - 1].Range.Start))
                {
                    result.Add(new Mistake()
                    {
                        Description = GetDescription(),
                        Position = 3,
                        Level = MistakeLevel.Error,
                        Text = $"Неправильне розміщення розділу: {part.Caption}"
                    });
                }
            }

            return result;
        }

        public override string ErrorDetails { get { return "ContentConformityRule"; } set { } }
        public override string GetDescription()
        {
            return "Робота повинна містити усі задані розділи, мати правильний стиль Заголовок 1 і написана великими буквами наприклад РОЗДІЛ 1. ВИСНОВКИ ";
        }
        public string GetDescription2()
        {
            return "Робота повинна містити розділи в строгому порядку";
        }
    }
}
