﻿using System;
using System.Collections.Generic;
using System.Globalization;
using DocumentChecker.Model.Annotations;
using Microsoft.Office.Interop.Word;

namespace DocumentChecker.Model.Rules
{
    /// <summary>
    /// class for Literature Reference
    /// </summary>
    [Serializable]
    public class LiteratureReferenceRule : Rule
    {
        private int Count { get; set; }
        private bool[] Reference { get; set; }
        const string leftSymbol = "[";
        const string rightSymbol = "]";
        bool leftFound = false;
        int leftIndex = -1;
        /// <summary>
        /// Constructor of class
        /// </summary>
        public LiteratureReferenceRule()
        {

        }

        public void PrepareLitList(DocPart docPart)
        {
            Count = docPart.Range.ListParagraphs.Count;
            // Count = LiteratureListRule.GetListCount();
            if (Count > 0)
            {
                Reference = new bool[Count];
                for (int i = 0; i < Count; i++)
                {
                    Reference[i] = false;
                }
            }
        }
        public override List<Mistake> FindMistakes(DocPart docPart)
        {
            PrepareLitList(docPart);
            if (Count <= 0)
            {
                const string err = "Не знайдено список літератури";
                return new List<Mistake> { new Mistake { Text = err, Position = docPart.Range.Start, Level = MistakeLevel.Error, Description = GetDescription() } };
            }
            //Find '[' and ']'       
            var mistakeList = new List<Mistake>();
            try
            {
                for (int i = 0; i < docPart.Range.Text.Length; i++)
                {
                    if (docPart.Range.Text[i].ToString(CultureInfo.InvariantCulture) == leftSymbol)
                    {//found [
                        leftFound = true;
                        leftIndex = i + 1;
                    }
                    if (docPart.Range.Text[i].ToString(CultureInfo.InvariantCulture) == rightSymbol & leftFound)
                    {
                        //found ]
                        int rightIndex = i - 1;
                        string insideString = docPart.Range.Text.Substring(leftIndex, rightIndex - leftIndex + 1);
                        var mistake = CheckString(insideString, i, docPart);
                        mistakeList.Add(mistake);
                    }
                }
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch (Exception)
            // ReSharper restore EmptyGeneralCatchClause
            {
                //MAGIC
            }

            mistakeList.AddRange(CheckReferenceExistance(docPart));
            return mistakeList;
        }

        /// <summary>
        /// Check if literature element has reference in the text
        /// </summary>
        /// <param name="docPart"> Document Part</param>
        /// <returns>List of Mistakes</returns>
        private IEnumerable<Mistake> CheckReferenceExistance(DocPart docPart)
        {
            var list = new List<Mistake>();
            for (int i = 0; i < Count; i++)
            {

                if (Reference[i] == false)
                {
                    var mistake = new Mistake
                        {
                            Description = GetDescription(),
                            Level = MistakeLevel.Error,
                            Text = "Відсутнє посилання на джерело " + (i + 1).ToString(CultureInfo.InvariantCulture),
                            Position = docPart.Range.Start
                        };
                    list.Add(mistake);
                }
            }
            return list;

        }


        /// <summary>
        /// Check string, if it has numbers
        /// </summary>
        /// <param name="iString">input String for checking</param>
        /// <param name="position">Position of string input in text for error details</param>
        /// <param name="docPart">Document</param>
        /// <returns>Mistake</returns>
        public Mistake CheckString(string iString, int position, DocPart docPart)
        {
            string[] strings = iString.Split(' ', '-', ',');
            //active page number
            var page = (int)docPart.Range.Information[WdInformation.wdActiveEndAdjustedPageNumber];
            foreach (var s in strings)
            {
                int value;
                bool b = int.TryParse(s, out value);
                if (b)
                {
                    if (value > Count | value <= 0)
                    {
                        return new Mistake
                            {
                                Description = GetDescription(),
                                Text = "Посилання на літературу вказано невірно",
                                Level = MistakeLevel.Error,
                                Position = page
                            };
                    }
                    Reference[value - 1] = true;
                }
            }
            return new Mistake();
        }


        public override string ErrorDetails { get { return "LiteratureReferenceRule"; } set { } }
        public override string GetDescription()
        {
            const string description = @"Посилання повинно вказувати на існуюче джерело в списку літератури.";
            return description;
        }
    }
}
