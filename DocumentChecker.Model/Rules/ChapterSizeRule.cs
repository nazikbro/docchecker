﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DocumentChecker.Model.Annotations;
using Word = Microsoft.Office.Interop.Word;
using System.Diagnostics;
namespace DocumentChecker.Model.Rules
{
    ///<summary>
    /// Class for checking size of chapter
    /// </summary>
    [Serializable]
    public class ChapterSizeRule : Rule
    {
        public int MinPageCount { get; set; }
        public int MaxPageCount { get; set; }

        public override List<RuleField> GetField()
        {
            return new List<RuleField>
            {
                new RuleField("Мінімальна к-сть сторінок", MinPageCount.ToString()),
                new RuleField("Максимальна к-сть сторінок", MaxPageCount.ToString())
            };
        }

        public override void SetField(List<RuleField> fields)
        {
            try
            {
                MinPageCount = int.Parse(fields[0].FieldText);
                MaxPageCount = int.Parse(fields[1].FieldText);
            }
            catch (Exception)
            {
                MessageBox.Show("Виникла помилка! Неправильно заповнені поля!");
            }
        }

        /// <summary>
        /// Constructor of class
        /// </summary>
        /// <param name="minPageCount">minimum count of page's in chapter</param>
        /// <param name="maxPageCount">maximum count of page's in chapter</param>
        public ChapterSizeRule(int minPageCount, int maxPageCount)
        {
            MinPageCount = minPageCount;
            MaxPageCount = maxPageCount;
        }

        public override List<Mistake> FindMistakes(DocPart docPart)
        {
            //Checking size of chapter
            try
            {
                const Word.WdStatistic stat = Word.WdStatistic.wdStatisticPages;//getting statistics of document
                object missing = System.Reflection.Missing.Value; //object of search
                int num = docPart.Range.ComputeStatistics(stat);//count of page's
                int count = num;
                var message = GetDescription();
                // get number of pages
                //active page number
                var page = (int)docPart.Range.Information[Word.WdInformation.wdActiveEndAdjustedPageNumber];
                if (count < MinPageCount)
                {
                    return new List<Mistake> { new Mistake { Description = message, Text = "Обсяг" + docPart.Caption + " надто малий", Level = MistakeLevel.Warning, Position = page } };
                }
                if (count > MaxPageCount)
                {
                    return new List<Mistake> { new Mistake { Description = message, Text = "Обсяг " + docPart.Caption + " надто великий", Level = MistakeLevel.Warning, Position = page } };
                }
                return new List<Mistake>();
            }
            // ReSharper disable EmptyGeneralCatchClause
            catch
            // ReSharper restore EmptyGeneralCatchClause
            {

            }
            return new List<Mistake>();
        }

        public override string ErrorDetails { get { return "ChapterSizeRule"; } set { } }
        public override string GetDescription()
        {
            const string description = @"Розмір розділу повинен відповідати певному обсягу
, що заданий мінімальною та максимальною кількістю сторінок";
            return description;
        }
    }
}
