﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using DocumentChecker.Model.Annotations;
using DocumentChecker.Model.Helper;
using Microsoft.Office.Interop.Word;

namespace DocumentChecker.Model
{
    /// <summary>
    /// абстрактний клас частини документу
    /// </summary>
    [Serializable]
    public abstract class DocPart
    {
        /// <summary>
        /// Text to preview in the program
        /// </summary>
        public string Caption{get;set;}

        public List<Rule> Rules;
        public List<DocPart> Parts;
        [NonSerialized]
        public Range Range;
        [NonSerialized] 
        public Document Document;
        
        public void LoadRules(string path)
        {
            Rules = new List<Rule>();
            Parts = new List<DocPart>();
            try
            {
                using (Stream stream = File.Open(path, FileMode.Open))
                {
                    BinaryFormatter bin = new BinaryFormatter();

                    DocPart docPart = (DocPart)bin.Deserialize(stream);
                    Rules = docPart.Rules;
                    Parts = docPart.Parts;
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex,"DocPart");
            }
        }

        public void SaveRules(string path)
        {
            try
            {
                using (Stream stream = File.Open(path, FileMode.Create))
                {
                    BinaryFormatter bin = new BinaryFormatter();
                    bin.Serialize(stream, this);
                }
            }
            catch (IOException ex)
            {
                Logger.Error(ex, "DocPart");
            }
        }
    }
}
