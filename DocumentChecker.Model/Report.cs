﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace DocumentChecker.Model
{
    public class Report
    {
        public static void BuildReport(IEnumerable<Mistake> list)
        {

            list.OrderBy(x => x.Level);
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "html file (*.html)|*.html";
            dlg.Title = "Оберіть html файл ........ для звіту";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                FileStream fs = new FileStream(dlg.FileName, FileMode.OpenOrCreate);
                System.IO.StreamWriter wr = new StreamWriter(fs);
                wr.WriteLine("<html><head><meta content=\"text/html; charset=utf-8\" http-equiv=\"Content-Type\" /><title>Звіт по помилках</title></head><body bgcolor=\"#FFFF99\"><H1 align=\"center\">Звіт</H1><ul>");
                foreach (var mistake in list)
                {
                    switch (mistake.Level)
                    {
                        case MistakeLevel.Error:
                            {
                                wr.WriteLine("<li><p style=\"color:red\">Помилка :" + mistake.Text + @"</p><p>" + mistake.Description + "</p></li>");
                                break;
                            }
                        case MistakeLevel.Warning:
                            {
                                wr.WriteLine("<li><p style=\"color:green\">Попередження :" + mistake.Text + @"</p><p>" + mistake.Description + "</p></li>");
                                break;
                            }
                        case MistakeLevel.Information:
                            {
                                wr.WriteLine("<li><p style=\"color:blue\">Зверни увагу :" + mistake.Text + @"</p><p>" + mistake.Description + "</p></li>");
                                break;
                            }
                    }

                }
                wr.WriteLine(@"</ul></body></html>");
                wr.Close();
                fs.Close();
            }
        }
        public static string BuildTempReport(IEnumerable<Mistake> list)
        {
            var path = @"temp/";
            var fileName = "report.html";
            list.OrderBy(x => x.Level);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            var fs = new FileStream(path + fileName, FileMode.Create);
            var wr = new StreamWriter(fs);
            wr.WriteLine("<html><head><meta content=\"text/html; charset=utf-8\" http-equiv=\"Content-Type\" /><title>Звіт по помилках</title></head><body bgcolor=\"#FFFF99\"><H1 align=\"center\">Звіт</H1><ul>");
            foreach (var mistake in list)
            {
                switch (mistake.Level)
                {
                    case MistakeLevel.Error:
                        {
                            wr.WriteLine("<li><p style=\"color:red\">Помилка :" + mistake.Text + @"</p><p>" + mistake.Description + "</p></li>");
                            break;
                        }
                    case MistakeLevel.Warning:
                        {
                            wr.WriteLine("<li><p style=\"color:green\">Попередження :" + mistake.Text + @"</p><p>" + mistake.Description + "</p></li>");
                            break;
                        }
                    case MistakeLevel.Information:
                        {
                            wr.WriteLine("<li><p style=\"color:blue\">Зверни увагу :" + mistake.Text + @"</p><p>" + mistake.Description + "</p></li>");
                            break;
                        }
                }

            }
            wr.WriteLine(@"</ul></body></html>");
            wr.Close();
            fs.Close();
            return path + fileName;
        }
    }
}
