﻿using System.Text;

namespace DocumentChecker.WebModule
{
    static class Constant
    {
        public static readonly string API_SERVER_BASE_PATH = "http://localhost:8080";
        public static readonly string API_BASE_PREFIX = "/api/public";
        public static readonly string GET_DEPARTMENTS = "getDepartments";
        public static readonly string GET_EMPLOYEES_BY_DEPARTMENT_0 = "getEmployees?id_department={0}";
        public static readonly string GET_DOCUMENT_SPECIFICATIONS = "getSpecifications";

        public static readonly string UPLOAD_FILE = "uploadReport";
        public static readonly string DOWNLOAD_FILE_BY_ID_0 = "downloadSpecification?documentId={0}";

        public static readonly Encoding ENCODING = Encoding.UTF8;
    }
}
