﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using DocumentChecker.WebModule;

namespace DocumentChecker.WebModule
{
    enum HttpMethod
    {
        Get,
        Post,
        Put,
        Delete
    }

    class RestClient
    {
        private static string GetBaseURL()
        {
            return Constant.API_SERVER_BASE_PATH + Constant.API_BASE_PREFIX;
        }

        internal static Stream MakeGetRequest(string methodName)
        {
            var request = (HttpWebRequest)WebRequest.Create(GetBaseURL() + "/" + methodName);
            return executeRequest(request);
        }

        private static Stream executeRequest(HttpWebRequest request)
        {
            try
            {
                var response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK) return response.GetResponseStream();

                var message = String.Format("Request failed. Received HTTP {0}", response.StatusCode);
                return null;
            }
            catch (WebException e)
            {
                var message = String.Format("Request failed. Received HTTP {0}", e.StackTrace);
                return null;
            }



        }

        internal static void DownloadFile(string filePath, string methodName/*DownloadProgressChangedEventHandler downloadProgressEvent*/)
        {
            using (var wc = new WebClient())
            {
                //wc.DownloadProgressChanged += downloadProgressEvent;
                try
                {
                    var uri = new System.Uri(GetBaseURL() + "/" + methodName);
                    wc.DownloadFileAsync(uri, filePath);
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.StackTrace);
                }
            }
        }

    }
}
