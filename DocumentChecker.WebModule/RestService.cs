﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using DocumentChecker.WebModule.Entity;

namespace DocumentChecker.WebModule
{
    public class RestService
    {
        public static List<Department> LoadDepartments()
        {
            var responseStream = RestClient.MakeGetRequest(Constant.GET_DEPARTMENTS);

            return responseStream != null ? JsonConverter.ConvertToObjectList<Department>(responseStream) : null;
            //todo refactor
        }

        public static List<Employee> LoadEmployees(long id)
        {
            var responseStream = RestClient.MakeGetRequest(string.Format(Constant.GET_EMPLOYEES_BY_DEPARTMENT_0, id));

            return responseStream != null ? JsonConverter.ConvertToObjectList<Employee>(responseStream) : null;
            //todo refactor
        }

        public static void UpdateDocumentSpecifications(string path)
        {
            var responseStream = RestClient.MakeGetRequest(Constant.GET_DOCUMENT_SPECIFICATIONS);
            List<DocumentSpecification> documentSpecifications = null;
            //responseStream = RestClient.MakeGetRequest(string.Format(Constant.GET_EMPLOYEES_BY_DEPARTMENT_0, id));

            if (responseStream == null) return;

            documentSpecifications = JsonConverter.ConvertToObjectList<DocumentSpecification>(responseStream);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            else
            {
                var localFilePaths = Directory.GetFiles(path);
                var localFileNames = localFilePaths.Select(Path.GetFileName).ToList();
                documentSpecifications.RemoveAll(
                    specification => localFileNames.Contains(specification.documentName));
            }

            foreach (var document in documentSpecifications)
            {
                RestClient.DownloadFile(path + "/" + document.documentName, string.Format(Constant.DOWNLOAD_FILE_BY_ID_0, document.id));
            }
        }
        public static SingleResponse UploadStudentReport(StudentReport studentReport, string documentPath, string reportPath)
        {
            var uri = new Uri(Constant.API_SERVER_BASE_PATH + Constant.API_BASE_PREFIX + "/" + Constant.UPLOAD_FILE);
            var result = RestFileLoader.UploadStudenReport(uri, studentReport, documentPath, reportPath).Result;

            try
            {
                return JsonConverter.ConvertToObject<SingleResponse>(result);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.StackTrace);
                return new SingleResponse { message = "Json parse error" };
            }
        }
    }
}
