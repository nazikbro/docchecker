﻿namespace DocumentChecker.WebModule
{
    public class Employee
    {
        public long id_employee { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string patronymic { get; set; }
    }
}
