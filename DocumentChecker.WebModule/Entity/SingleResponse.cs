﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentChecker.WebModule.Entity
{
    /// <summary>
    /// DTO class for parsing json response from server
    /// </summary>
    public class SingleResponse
    {
        public bool success { get; set; }

        public long errorCode { get; set; }

        public string message { get; set; }

    }
}
