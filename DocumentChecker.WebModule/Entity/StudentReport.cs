﻿
// ReSharper disable InconsistentNaming

namespace DocumentChecker.WebModule.Entity
{
    public class StudentReport
    {
        public string studentInfo { get; set; }

        public string studentEmail { get; set; }

        public string studentGroup { get; set; }

        public string employeeId { get; set; }

        public StudentReport(string employeeId,string studentInfo, string studentEmail, string studentGroup)
        {
            this.employeeId = employeeId;
            this.studentInfo = studentInfo;
            this.studentEmail = studentEmail;
            this.studentGroup = studentGroup;
        }
    }
}
