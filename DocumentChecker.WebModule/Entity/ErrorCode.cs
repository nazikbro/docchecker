﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentChecker.WebModule.Entity
{
    enum ErrorCode
    {
        INCORRECT_USERNAME = 1,
        INCORRECT_PASSWORD = 2,
        REPORT_NOT_FOUND = 3,
        RECORD_NOT_FOUND = 4,
        RATE_NOT_FOUND = 5,
        EMPLOYEE_NOT_FOUND = 6,
        NOT_ENOUGH_PERMISSIONS = 7,
        STATUS_NOT_FOUND = 8,
        UNAUTHORIZED = 9,
        WRONG_VALUE_PROVIDED = 10,
        UNKNOWN_ERROR = 11,
        CATEGORY_NOT_FOUND = 12,
        COEFFICIENT_NOT_FOUND = 13,
        DATABASE_ERROR = 14,
        GENERAL_ERROR = 15,
        STUDY_YEAR_NOT_FOUND = 16,
        NO_ERROR = -1

    }
}
