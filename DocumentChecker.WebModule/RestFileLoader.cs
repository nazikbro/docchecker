﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Net.Http;
using DocumentChecker.WebModule.Entity;

namespace DocumentChecker.WebModule
{
    /// <summary>
    /// Class for uploading and downloading files from/to Rest service
    /// </summary>
    class RestFileLoader
    {
        /// <summary>
        /// Upload srudent report to server
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="studentReport"></param>
        /// <param name="fileFullPath"></param>
        /// <returns></returns>
        internal static async Task<string> UploadStudenReport(Uri uri, StudentReport studentReport, string documentPath, string reportPath)
        {

            var documentInfo = new FileInfo(documentPath);
            var documentName = documentInfo.Name;
            var documentFileContents = GetFileBytesArray(documentInfo);


            var reportInfo = new FileInfo(reportPath);
            var reportName = reportInfo.Name;
            var reportFileContents = GetFileBytesArray(reportInfo);

            using (var content =
                new MultipartFormDataContent("Upload student report"))
            {
                content.Add(new StreamContent(new MemoryStream(documentFileContents)), "document", documentName);
                content.Add(new StreamContent(new MemoryStream(reportFileContents)), "report", reportName);
                content.Add(new StringContent(studentReport.studentInfo, Constant.ENCODING), "studentInfo");
                content.Add(new StringContent(studentReport.studentEmail, Constant.ENCODING), "studentEmail");
                content.Add(new StringContent(studentReport.studentGroup, Constant.ENCODING), "studentGroup");
                content.Add(new StringContent(studentReport.employeeId, Constant.ENCODING), "employeeId");
                return await UploadFile(uri, content).ConfigureAwait(false);
            }
        }

        /// <summary>
        /// Convert file to bytes array
        /// </summary>
        /// <param name="fileInfo"></param>
        /// <returns></returns>
        private static byte[] GetFileBytesArray(FileInfo fileInfo)
        {
            try
            {
                return File.ReadAllBytes(fileInfo.FullName);
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.StackTrace);
                return null;
            }
        }

        /// <summary>
        /// Upload multipart contetn to server
        /// </summary>
        /// <param name="uri">Service uri to upload file</param>
        /// <param name="content">Content to send</param>
        /// <returns></returns>
        private static async Task<string> UploadFile(Uri uri, MultipartFormDataContent content)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    await Task.Delay(TimeSpan.FromSeconds(3)).ConfigureAwait(false);
                    var message = await client.PostAsync(uri, content).ConfigureAwait(false);
                    
                    var input = await message.Content.ReadAsStringAsync().ConfigureAwait(false);

                    return !string.IsNullOrWhiteSpace(input) ? input : null;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
                return null;
            }
        }
    }
}
