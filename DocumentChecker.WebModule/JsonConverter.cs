﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DocumentChecker.WebModule
{
     class JsonConverter
    {
        public static List<T> ConvertToObjectList<T>(string json)
        {
            var jArray = JArray.Parse(json);
            return jArray.Select(token => JsonConvert.DeserializeObject<T>(token.ToString())).ToList();
        }

        public static List<T> ConvertToObjectList<T>(Stream stream)
        {
            return ConvertToObjectList<T>(new StreamReader(stream).ReadToEnd());
        }

         public static string ConvertToJson(object jObject)
         {
             return JsonConvert.SerializeObject(jObject);
         }

        public static T ConvertToObject<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}
