﻿using DocumentChecker.Model.Docs;
using DocumentChecker.Model.Rules;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace DocumentChecker.Model.Tests
{
    [TestClass]
    public class NonTypicalTextRuleTest
    {
        [TestMethod]
        public void NonTypicalTextRuleT()
        {
            var diploma = new TheDocumentContent();
            diploma.LoadDoc("C:ddd", "test");
            var rule = new NonTypicalTextRule(new List<string>() { "Література" });
            var mistakes = rule.FindMistakes(diploma);
            foreach (var mistake in mistakes)
            {
                Debug.WriteLine(mistake.Text);
                Debug.WriteLine(mistake.Description);
            }
        }
    }
}
