﻿using System.Diagnostics;
using DocumentChecker.Model.Docs;
using DocumentChecker.Model.Rules;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DocumentChecker.Model.Tests
{
    [TestClass]
    public class ChapterSizeTest
    {
        [TestMethod]
        public void ChapterSizeRuleTest()
        {
            var diploma = new TheDocumentContent();
            diploma.LoadDoc("C:ddd", "test");
            var rule = new ChapterSizeRule(5,8);
            var mistakes = rule.FindMistakes(diploma);
            foreach (var mistake in mistakes)
            {
                Debug.WriteLine(mistake.Text);
            }
        }
    }
}
