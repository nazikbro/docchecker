﻿using System.Diagnostics;
using DocumentChecker.Model.Docs;
using DocumentChecker.Model.Rules;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DocumentChecker.Model.Tests
{
    [TestClass]
    public class TextFormattingTest
    {
        [TestMethod]
        public void TextFormattingTest1()
        {
            var diploma = new TheDocumentContent();
            diploma.LoadDoc("C:ddd", "test");
            const int size = 14;
            const int bold = 0;
            const int italic = 0;
            const float lineSpacing = 18f;
            var rule = new TextFormattingRule(size,bold,italic,lineSpacing,"Times New Roman");
            var mistakeList = rule.FindMistakes(diploma);
            foreach (var mistake in mistakeList)
            {
                Debug.WriteLine(mistake.Text);
            }
        }
    }
}
