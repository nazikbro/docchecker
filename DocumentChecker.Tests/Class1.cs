﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DocumentChecker.Model.Docs;
using DocumentChecker.Model.Parts;
using DocumentChecker.Model.Rules;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace DocumentChecker.Model.Tests
{
    [TestClass]
    public class UnitTest2
    {
       
      /*  [TestMethod]
        public void TestPageNumeration()
        {
            Diploma d = new Diploma("D:\\1.doc");
            PageNumerationRule p=new PageNumerationRule();
            p.FindMistakes(d.Parts[0]);
        }*/
/*
        [TestMethod]
        public void TestFormula()
        {
            var d = new Diploma(@"G:/1.docx");
            var rule=new FormulaAlignRule();
           // f.CheckShit(d.Range);
          var mistakes=  rule.FindMistakes(d);
            foreach (var mistake in mistakes)
            {
                Debug.WriteLine(mistake.Text);
            }
        }*/

        [TestMethod]
        public void HTML()
        {
            List<Mistake> ms=new List<Mistake>();
            ms.Add(new Mistake(){Level = MistakeLevel.Error,
                                         Position = 0,
                                         Text = "Помилка в нумерації сторінок (має починатись з 2)",
                                         Description = "Нумерація сторінок не має відображатись на першій сторінці або неправильний початковий номер"
                                     });
            ms.Add(new Mistake()
            {
                Level = MistakeLevel.Warning,
                Position = 0,
                Text = "Помилка в нумерації сторінок (має починатись з 2)",
                Description = "Нумерація сторінок не має відображатись на першій сторінці або неправильний початковий номер"
            });
            ms.Add(new Mistake()
            {
                Level = MistakeLevel.Information,
                Position = 0,
                Text = "Помилка в нумерації сторінок (має починатись з 2)",
                Description = "Нумерація сторінок не має відображатись на першій сторінці або неправильний початковий номер"
            });
            Report.BuildReport(ms);
        }
    }
}
