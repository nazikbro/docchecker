﻿using DocumentChecker.Model.Docs;
using DocumentChecker.Model.Rules;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace DocumentChecker.Model.Tests
{
    [TestClass]
    public class LiteratureRequiredReferenceRuleTest
    {
        [TestMethod]
        public void LitReqRefRuleTeatMethod()
        {
            var diploma = new TheDocumentContent();
            diploma.LoadDoc("C:ddd", "test");
            var rule = new RequiredLiteratureReferenceRule();
            var mistakes = rule.FindMistakes(diploma);
            foreach (var mistake in mistakes)
            {
                Debug.WriteLine(mistake.Text);
            }
        }
    }
}
