﻿using System.Diagnostics;
using DocumentChecker.Model.Docs;
using DocumentChecker.Model.Rules;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DocumentChecker.Model.Tests
{
    [TestClass]
    
    public class LiteratureListTest
    {
        [TestMethod]
        public void CheckRules()
        {
            var diploma = new TheDocumentContent();
            diploma.LoadDoc("C:ddd", "test");
            var rule = new LiteratureListRule();
            var mistakeList=rule.FindMistakes(diploma);
            foreach (var mistake in mistakeList)
            {
             Debug.WriteLine(mistake.Text+":"+mistake.Position); 
            }
        }
    }
}
