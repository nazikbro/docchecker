﻿using System.Diagnostics;
using DocumentChecker.Model.Docs;
using DocumentChecker.Model.Rules;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DocumentChecker.Model.Tests
{
    [TestClass]
    public class AdditionalsTest
    {
        [TestMethod]
        public void AdditionalsRuleTest()
        {
            var diploma = new TheDocumentContent();
            diploma.LoadDoc("C:ddd", "test");
            var rule = new AdditionalsRule();
            var mistakes = rule.FindMistakes(diploma);
            foreach (var mistake in mistakes)
            {
                Debug.WriteLine(mistake.Text);
            }
        }
    }
}
