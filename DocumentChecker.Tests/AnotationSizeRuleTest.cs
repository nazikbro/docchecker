﻿using System.Diagnostics;
using DocumentChecker.Model.Docs;
using DocumentChecker.Model.Rules;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DocumentChecker.Model.Tests
{
    [TestClass]
    public class AnotationSizeRuleTest
    {
        [TestMethod]
        public void AnotationSizeRuleTestMethod()
        {
            var diploma = new TheDocumentContent();
            diploma.LoadDoc("C:ddd", "test");
            var rule = new AnnotationSizeRule();
            var mistakes = rule.FindMistakes(diploma);
            foreach (var mistake in mistakes)
            {
                Debug.WriteLine(mistake.Text);
            }
        }
    }
}
