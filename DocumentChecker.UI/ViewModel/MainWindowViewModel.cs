﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using DocumentChecker.Model;
using DocumentChecker.Model.Docs;
using DocumentChecker.Model.Helper;
using DocumentChecker.WebModule;
using Microsoft.Win32;

namespace DocumentChecker.UI.ViewModel
{
    public class MainWindowViewModel : ViewModelBase
    {
        private readonly IteratorMistake mistakeIterator;

        private static string DocumentTemplatePath = @"" + Properties.Settings.Default.TemplatesDirectory;

        public MainWindowViewModel()
        {
            mistakeIterator = new IteratorMistake();
            mistakeIterator.MistakeFounded += FindMistakeEvent;
            Mistakes = new List<Mistake>();

            CheckDocument = new Command(arg => DoSearch());
            ShowAbout = new Command(arg => showAbout());
            ShowMainWin = new Command(arg => showMainWin());
            OpenFile = new Command(arg => openFile());
            ShowProperties = new Command(arg => showProperties());
            SendDocument = new Command(arg => sendDocument());
            SendReportCommand = new Command(arg => sendReportToServer());
            UploadFormServerCommand = new Command(arg => ReloadFromServer());
            ReloadCommand = new Command(arg => Reload());

            Reload();

            PathText = "Перенесіть сюди файл";
            docTypeValue = "Дипломна";
            ProgressVisibility = Visibility.Collapsed;
            TextOfCompleteVisibility = Visibility.Collapsed;

            IsTextButEnable = false;
            IsSearchButEnabled = false;
            IsSendButEnabled = false;
            IsOpenButEnable = true;
            SearchButTip =
                "Пошук по документі\n\n  *Для активації виберіть документ або перетягніть його в текстове поле";
            SendButTip = "Надіслати звіт\n\n  *Для активації зробіть пошук помилок по документі";
            FUEOb = "OpenBut";
        }

        #region ICommands
        public ICommand CheckDocument { get; set; }
        public ICommand SendDocument { get; set; }
        public ICommand ShowAbout { get; set; }
        public ICommand ShowMainWin { get; set; }
        public ICommand ShowHelp { get; set; }
        public ICommand OpenFile { get; set; }
        public ICommand ShowProperties { get; set; }
        public ICommand OnLoad { get; set; }
        public ICommand SendReportCommand { get; set; }
        public ICommand UploadFormServerCommand { get; set; }
        public ICommand ReloadCommand { get; set; }

        #endregion

        private void FindMistakeEvent(object o, MistakeEventAgrs args)
        {
            OnPropertyChanged("Mistakes");
        }

        public void ReloadFromServer()
        {
            RestService.UpdateDocumentSpecifications(DocumentTemplatePath);
            Reload();
        }

        public void Reload()
        {
            if (!Directory.Exists(DocumentTemplatePath))
            {
                Directory.CreateDirectory(DocumentTemplatePath);
            }
            var files = Directory.GetFiles(DocumentTemplatePath);
            DocTypeList = new ObservableCollection<string>();

            foreach (var file in files)
            {
                var start = file.LastIndexOf('\\') + 1;
                var count = file.LastIndexOf('.') - start;
                //if (count > 0)
               // {
                    var name = file.Substring(start, count);
                    DocTypeList.Add(name);
              //  }
               // DocTypeList.Add(file);
            }
        }

        public void DoSearch()
        {
            ControlofStatus();

            TextOfCompleteVisibility = Visibility.Collapsed;
            try
            {
                var thread = new Thread(GetMistakes) {IsBackground = true};
                thread.Start();
            }
            catch (Exception ex)
            {
                Logger.Error(ex,"MainWindowViewModule");
            }
        }

        #region Properties

        // ReSharper disable InconsistentNaming
        private bool isOpenButEnable { get; set; }

        public bool IsOpenButEnable
        {
            get { return isOpenButEnable; }
            set
            {
                isOpenButEnable = value;
                OnPropertyChanged("IsOpenButEnable");
            }
        }

        private List<Mistake> _mistakes = new List<Mistake>();

        public List<Mistake> Mistakes
        {
            get { return _mistakes; }
            set
            {
                _mistakes = value;
                OnPropertyChanged("Mistakes");
            }
        }

        private string fueOb { get; set; }

        public string FUEOb
        {
            get { return fueOb; }
            set
            {
                fueOb = value;
                OnPropertyChanged("FUEOb");
            }
        }

        private string pathText;

        public string PathText
        {
            get { return pathText; }
            set
            {
                pathText = value;
                OnPropertyChanged("PathText");
            }
        }

        private ObservableCollection<string> docTypeList { get; set; }

        public ObservableCollection<string> DocTypeList
        {
            get { return docTypeList; }
            set
            {
                docTypeList = value;
                OnPropertyChanged("DocTypeList");
            }
        }

        private string docTypeValue { get; set; }

        public string DocTypeValue
        {
            get { return docTypeValue; }
            set
            {
                docTypeValue = value;
                OnPropertyChanged("DocTypeValue");
            }
        }

        private int worningCount { get; set; }

        public int WorningCount
        {
            get { return worningCount; }
            set
            {
                worningCount = value;
                OnPropertyChanged("WorningCount");
            }
        }

        private int errorCount { get; set; }

        public int ErrorCount
        {
            get { return errorCount; }
            set
            {
                errorCount = value;
                OnPropertyChanged("ErrorCount");
            }
        }

        private int informCount { get; set; }

        public int InformCount
        {
            get { return informCount; }
            set
            {
                informCount = value;
                OnPropertyChanged("InformCount");
            }
        }

        private double progressValue { get; set; }

        public double ProgressValue
        {
            get { return progressValue; }
            set
            {
                progressValue = value;
                OnPropertyChanged("ProgressValue");
            }
        }

        private Visibility textOfCompleteVisibility { get; set; }

        public Visibility TextOfCompleteVisibility
        {
            get { return textOfCompleteVisibility; }
            set
            {
                textOfCompleteVisibility = value;
                OnPropertyChanged("TextOfCompleteVisibility");
            }
        }

        private Visibility progressVisibility { get; set; }

        public Visibility ProgressVisibility
        {
            get { return progressVisibility; }
            set
            {
                progressVisibility = value;
                OnPropertyChanged("ProgressVisibility");
            }
        }


        private bool isTextButEnabled { get; set; }

        public bool IsTextButEnable
        {
            get { return isTextButEnabled; }
            set
            {
                isTextButEnabled = value;
                OnPropertyChanged("IsTextButEnabled");
            }
        }

        private bool isSearchButEnabled { get; set; }

        public bool IsSearchButEnabled
        {
            get { return isSearchButEnabled; }
            set
            {
                isSearchButEnabled = value;
                OnPropertyChanged("IsSearchButEnabled");
            }
        }

        private bool isSendButEnabled { get; set; }

        public bool IsSendButEnabled
        {
            get { return isSendButEnabled; }
            set
            {
                isSendButEnabled = value;
                OnPropertyChanged("IsSendButEnabled");
            }
        }

        private string searchButTip { get; set; }

        public string SearchButTip
        {
            get { return searchButTip; }
            set
            {
                searchButTip = value;
                OnPropertyChanged("SearchButTip");
            }
        }

        private string sendButTip { get; set; }

        public string SendButTip
        {
            get { return sendButTip; }
            set
            {
                sendButTip = value;
                OnPropertyChanged("SendButTip");
            }
        }

        // ReSharper restore InconsistentNaming

        #endregion

        #region Commands

        // ReSharper disable InconsistentNaming

        public void sendReportToServer()
        {
            Report.BuildTempReport(Mistakes);
            new SendReportWindow().ShowDialog();
        }
        public void sendDocument()
        {
            Report.BuildReport(Mistakes);
            //  Properties.Settings.Default.IsFUE = false;
            // Properties.Settings.Default.Save();
        }

        public void openFile()
        {
            var openDialog = new OpenFileDialog
            {
                Filter = "Файли MS Word (*.doc,.docx)|*.doc;*.docx|Всі файли (*.*)|*.*",
                Title = "Виберіть файл документу"
            };

            var showDialog = openDialog.ShowDialog();
            if (showDialog == null || !showDialog.Value) return;//todo;

            //todo refactor path of file to check
            PathText = openDialog.FileName;
            IsSearchButEnabled = true;
            IsSendButEnabled = false;
            SearchButTip = "Пошук по документі";
        }

        public void showMainWin()
        {
            var win = new MainWindow();
            win.Show();
        }

        public void showProperties()
        {
            var win = new PropertyWindow();
            win.ShowDialog();
        }

        public void showAbout()
        {
            var aboutBox1 = new AboutBox();
            aboutBox1.ShowDialog();
        }

        // ReSharper restore InconsistentNaming

        #endregion

        #region Func

        public void MistakeCount()
        {
            ErrorCount = Mistakes.Count(x => x.Level == MistakeLevel.Error);
            WorningCount = Mistakes.Count(x => x.Level == MistakeLevel.Warning);
            InformCount = Mistakes.Count(x => x.Level == MistakeLevel.Information);
        }

        public void ControlofStatus()
        {
            ProgressVisibility = ProgressVisibility == Visibility.Visible
                ? Visibility.Collapsed
                : Visibility.Visible;
        }

        private void GetMistakes()
        {

            MessageBox.Show("Перевірка займе більше 5 хв");
            Debug.WriteLine(
                "\n========================================================================= \n Початок виконання");
            var watch = Stopwatch.StartNew();

            Mistakes = null;

            var document = new TheDocumentContent();
            document.LoadDoc(PathText, DocumentTemplatePath + DocTypeValue + ".xml");

            Mistakes = mistakeIterator.GetAllMistakes(document);
            MistakeCount();
            ControlofStatus();
            TextOfCompleteVisibility = Visibility.Visible;

            IsSendButEnabled = true;
            SendButTip = "Надіслати звіт";
            watch.Stop();
            var elapsedMs = TimeSpan.FromMilliseconds(watch.ElapsedMilliseconds).ToString(@"hh\:mm\:ss\:fff");
            Debug.WriteLine("\n Повний час виконання:" + elapsedMs);
            MessageBox.Show("Повний час виконання:" + elapsedMs);
        }

        #endregion
    }
}