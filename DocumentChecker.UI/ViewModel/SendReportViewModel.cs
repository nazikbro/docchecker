﻿using System;
using DocumentChecker.WebModule;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using DocumentChecker.Model.Helper;
using DocumentChecker.UI.Util;
using DocumentChecker.WebModule.Entity;
using Microsoft.Win32;

namespace DocumentChecker.UI.ViewModel
{
    class SendReportViewModel : ViewModelBase
    {

        #region Properties

        private List<Department> departments { get; set; }
        public List<Department> Departments
        {
            get { return departments; }
            set
            {
                departments = value;
                OnPropertyChanged("Departments");
            }
        }

        private List<Employee> employees { get; set; }
        public List<Employee> Employees
        {
            get { return employees; }
            set
            {
                employees = value;
                OnPropertyChanged("Employees");
            }
        }

        private Employee selectedEmployee { get; set; }
        public Employee SelectedEmployee
        {
            get
            {
                return selectedEmployee;
            }
            set
            {
                selectedEmployee = value;
                OnPropertyChanged("SelectedEmployee");
            }
        }

        private Department selectedDepartment { get; set; }
        public Department SelectedDepartment
        {
            get
            {
                return selectedDepartment;
            }
            set
            {
                selectedDepartment = value;
                OnPropertyChanged("SelectedDepartment");
                // todo add thread
                loadEmployees(selectedDepartment.id_department);
            }
        }

        private string studentInfo { get; set; }
        public string StudentInfo
        {
            get { return studentInfo; }
            set
            {
                studentInfo = value;
                OnPropertyChanged("StudentInfo");
            }
        }

        private string studentEmail { get; set; }
        public string StudentEmail
        {
            get { return studentEmail; }
            set
            {
                studentEmail = value;
                OnPropertyChanged("StudentEmail");
            }
        }

        private string studentGroup { get; set; }
        public string StudentGroup
        {
            get { return studentGroup; }
            set
            {
                studentGroup = value;
                OnPropertyChanged("StudentGroup");
            }
        }

        private string reportPath { get; set; }
        public string ReportPath
        {
            get { return reportPath; }
            set
            {
                reportPath = value;
                OnPropertyChanged("ReportPath");
            }
        }

        private string documentPath { get; set; }
        public string DocumentPath
        {
            get { return documentPath; }
            set
            {
                documentPath = value;
                OnPropertyChanged("DocumentPath");
            }
        }


        #endregion

        #region Commands

        public ICommand SendReportCommand { get; set; }
        public ICommand OpenReportCommand { get; set; }
        public ICommand OpenDocumentCommand { get; set; }
        public ICommand CloseModalCommand { get; set; }

        #endregion

        public SendReportViewModel()
        {
            SendReportCommand = new Command(arg => SendReport());
            OpenReportCommand = new Command(arg => OpenReport());
            OpenDocumentCommand = new Command(arg => OpenDocument());
        
            new Thread(loadDepartments).Start();
        }

        private void loadDepartments()
        {
            Departments = RestService.LoadDepartments();
        }

        private void loadEmployees(long id)
        {
            Employees = RestService.LoadEmployees(id);
        }

        private void SendReport()
        {
            if (ReportPath != "" && DocumentPath != "")
            {
                var student = new StudentReport(SelectedEmployee.id_employee.ToString(), StudentInfo, studentEmail, studentGroup);
                SingleResponse response = null;
                try
                {
                    response = RestService.UploadStudentReport(student, DocumentPath, @"temp/report.html");

                    Debug.WriteLine(response.message);
                }
                catch (Exception ex)
                {
                    Logger.Error(ex, "SendReportViewModule");
                }

                if (response != null && response.success)
                {
                    MessageBox.Show("Ваш звіт було успішно надіслано");
                    var window = Utilities.GetWindowRef("SendReportWin");
                    window.Close();
                }
                else
                {
                    MessageBox.Show("Помилка надсилання");
                }
            }
           // new Thread(Send).Start();
        }

        private void Send()
        {
            
        }

        private void OpenReport()
        {
            var openDialog = new OpenFileDialog
            {
                Filter = "Файли MS Word (*.doc,.docx)|*.doc;*.docx|Всі файли (*.*)|*.*",
                Title = "Виберіть файл документу"
            };

            var showDialog = openDialog.ShowDialog();
            if (showDialog == null || !showDialog.Value) return;//todo;

            ReportPath = openDialog.FileName;
            
        }
        private void OpenDocument()
        {
            var openDialog = new OpenFileDialog
            {
                Filter = "Файли MS Word (*.doc,.docx)|*.doc;*.docx|Всі файли (*.*)|*.*",
                Title = "Виберіть файл документу"
            };

            var showDialog = openDialog.ShowDialog();
            if (showDialog == null || !showDialog.Value) return;

            DocumentPath = openDialog.FileName;
        }

    }
}
