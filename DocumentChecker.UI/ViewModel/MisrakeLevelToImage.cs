﻿using System;
using System.Globalization;
using System.Windows.Data;
using DocumentChecker.Model;

namespace DocumentChecker.UI.ViewModel
{
    class MisrakeLevelToImage:IValueConverter

    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
// ReSharper disable InconsistentNaming
            var Level = (MistakeLevel) value;
// ReSharper restore InconsistentNaming
            if (Level == MistakeLevel.Error)
                return "/Images/error.png";
            
            if (Level == MistakeLevel.Warning)
                return "/Images/warning.png";
            
            if (Level == MistakeLevel.Information)
                return "/Images/info.png";
            
            return  "/Images/warning.png";
        }
        
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
