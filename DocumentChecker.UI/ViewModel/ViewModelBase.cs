﻿using System.ComponentModel;

namespace DocumentChecker.UI.ViewModel
{
    /// <summary>
    /// Class for supporting property changing event
    /// </summary>
    public class ViewModelBase : INotifyPropertyChanged
    {
        #region PropertyChangedEventHandler

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
