﻿using DocumentChecker.Model;
using DocumentChecker.Model.Annotations;
using DocumentChecker.Model.Docs;
using DocumentChecker.Model.Parts;
using DocumentChecker.Model.Rules;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Command = DocumentChecker.Model.Command;
using Rule = DocumentChecker.Model.Rule;

namespace DocumentChecker.UI
{
    /// <summary>
    /// Interaction logic for DocTypesWindow.xaml
    /// </summary>
    public partial class DocTypesWindow : Window, INotifyPropertyChanged
    {
        // private TheDocumentContent document;

        public Command ShowDoc { get; set; }
        public Command SaveDoc { get; set; }
        public Command CreateDoc { get; set; }
        public Command AddChapterCommand { get; set; }
        public Command RemoveChapterCommand { get; set; }
        public Command EditChapterCommand { get; set; }
        public Command AddRuleCommand { get; set; }
        public Command RemoveRuleCommand { get; set; }

        private ObservableCollection<DocPart> _chapters;
        public ObservableCollection<DocPart> Chapters
        {
            get { return _chapters; }
            set
            {
                _chapters = value;
                OnPropertyChanged("Chapters");
            }
        }

        private DocPart SelectedChapter;
        private ObservableCollection<Rule> _rules;
        public ObservableCollection<Rule> Rules
        {
            get { return _rules; }
            set
            {
                _rules = value;
                foreach (var rule in _rules)
                {
                    var type = rule.GetType();
                    string name = TheDocumentContent.AllRules.First(c => c.Value == type).Key;
                    rule.RuleName = name;
                }
                OnPropertyChanged("Rules");
            }
        }

        private Rule SelectedRule;
        private ObservableCollection<RuleField> _ruleFields;
        public ObservableCollection<RuleField> RuleFields
        {
            get { return _ruleFields; }
            set
            {
                _ruleFields = value;
                OnPropertyChanged("RuleFields");
            }
        }

        private void CreateCommands()
        {
            ShowDoc = new Command(ShowDocAction);
            SaveDoc = new Command(SaveDocAction);
            CreateDoc = new Command(CreateDocAction);
            AddChapterCommand = new Command(AddChapterAction);
            AddRuleCommand = new Command(AddRuleAction);

            RemoveChapterCommand = new Command(RemoveChapterAction);
            RemoveRuleCommand = new Command(RemoveRuleAction);

            EditChapterCommand = new Command(EditChapterAction);
        }

        private void CreateDocAction(object obj)
        {
            Chapters = new ObservableCollection<DocPart>();
            Chapters.Add(new Chapter() { Caption = "Правила до документу", Rules = new List<Rule>() });
            Rules = new ObservableCollection<Rule>();
        }

        public DocTypesWindow(TheDocumentContent document)
        {
            Chapters = new ObservableCollection<DocPart>(document.Parts);
            Rules = new ObservableCollection<Rule>();
            InitializeComponent();

            DataContext = this;

            CreateCommands();
        }

        public DocTypesWindow()
        {
            InitializeComponent();
            DataContext = this;

            Chapters = new ObservableCollection<DocPart>();

            CreateCommands();

        }

        private void SaveDocAction(object obj)
        {
            var dialog = new SaveFileDialog
            {
                Title = "Choose save path:",
                Filter = "Файли xml (*.xml)|*.xml;|Всі файли (*.*)|*.*"
            };

            var showDialog = dialog.ShowDialog();
            if (showDialog == null || !showDialog.Value) return;

            TheDocumentContent document = new TheDocumentContent();

            var chapters = new List<DocPart>();
            var rules = new List<Rule>();
            foreach (var chapter in Chapters)
            {
                if (chapter.Caption != "Правила до документу")
                {
                    chapters.Add(chapter);
                }
                else rules = chapter.Rules;
            }
            document.Parts = chapters;
            document.Rules = rules;

            document.SaveRules(dialog.FileName);
        }

        public void ShowDocAction(object ob)
        {
            var dialog = new OpenFileDialog
            {
                Title = "Choose doctype file:",
                Filter = "Файли xml (*.xml)|*.xml;|Всі файли (*.*)|*.*"
            };

            if (dialog.ShowDialog().Value)
            {
                TheDocumentContent document = new TheDocumentContent();
                try
                {
                    document.LoadRules(dialog.FileName);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Wrong file was selected!");
                    return;
                }
                Chapters = new ObservableCollection<DocPart>();
                Chapters.Add(new Chapter() { Caption = "Правила до документу", Rules = document.Rules });
                foreach (var part in document.Parts)
                {
                    Chapters.Add(part);
                }

                Rules = new ObservableCollection<Rule>();
            }
        }

        public void AddChapterAction(object ob)
        {
            AdditionalWindow window = new AdditionalWindow("Введіть назву розділу", "Критерії пошуку", this);
            window.Show();
        }

        public void EditChapterAction(object ob)
        {
            AdditionalWindow window = new AdditionalWindow("Введіть назву розділу", "Критерії пошуку", SelectedChapter, this);
            window.Show();
        }

        public void AddRuleAction(object ob)
        {
            List<string> rulesList = new List<string>();
            rulesList.AddRange(TheDocumentContent.AllRules.Keys);
            AdditionalWindow window = new AdditionalWindow(rulesList, this);
            window.Show();
        }

        public void RemoveChapterAction(object ob)
        {
            if (!SelectedChapter.Caption.Contains("Правила до документу"))
            {
                var listCopy = new ObservableCollection<DocPart>(_chapters);
                listCopy.Remove(SelectedChapter);
                Chapters = listCopy;
            }
        }

        public void RemoveRuleAction(object ob)
        {
            var listCopy = new ObservableCollection<Rule>(_rules);
            listCopy.Remove(SelectedRule);
            Rules = listCopy;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void ChapterListBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                SelectedChapter = (e.AddedItems[0] as Chapter);
                Rules = new ObservableCollection<Rule>(SelectedChapter.Rules);
                RuleFields = null;
                SelectedRule = null;
            }
            catch (Exception ex)
            {
                Debug.Write(ex.Message);
            }
        }

        private void RuleListBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (SelectedRule != null)
                {
                    SelectedRule.SetField(new List<RuleField>(RuleFields));
                }
                SelectedRule = (e.AddedItems[0] as Rule);
                RuleFields = new ObservableCollection<RuleField>(SelectedRule.GetField());
            }
            catch (Exception ex)
            {
                Debug.Write(ex.Message);
            }
        }

        public void AddingChapter(string name, string startWith)
        {
            var ch = new List<DocPart>(Chapters);
            ch.Add(new Chapter()
            {
                Caption = name,
                ChapterTitle = new ChapterTitleTemplate() { StartWith = startWith },
                IsOptional = true,
                Rules = new List<Rule>()
            });
            Chapters = new ObservableCollection<DocPart>(ch);
        }

        public void AddingRule(string ruleName)
        {
            if (Rules != null)
            {
                var rules = new List<Rule>(Rules);
                //Type type = (TheDocumentContent.AllRules[ruleName]);
                Rule rule = null;
                #region RulesFinder
                if (ruleName == "Розмір розділу") rule = new ChapterSizeRule(0, 0);
                else
                if (ruleName == "Правопис") rule = new CheckSpelling();
                else
                if (ruleName == "Заголовки") rule = new HeadresStyleRule();
                else
                if (ruleName == "Список літератури") rule = new LiteratureListRule();
                else
                if (ruleName == "Розрив сторінки") rule = new PageBrakeRules();
                else
                if (ruleName == "Нумерування сторінок") rule = new PageNumerationRule();
                else
                if (ruleName == "Посилання на літературу") rule = new RequiredLiteratureReferenceRule();
                else
                if (ruleName == "Підписи рисунків") rule = new ShapeCaptionRule();
                else
                if (ruleName == "Підписи таблиць") rule = new TableCaptionRule();
                else
                if (ruleName == "Форматування тексту") rule = new TextFormattingRule(0, 0, 0, 0, "");
                else
                if (ruleName == "Всі вказані розділи") rule = new ContentConformityRule(); //11
                #endregion

                if (rule != null)
                {
                    rules.Add(rule);
                    Rules = new ObservableCollection<Rule>(rules);
                    SelectedChapter.Rules = rules;
                }
            }
        }

        private void RuleFieldsSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (SelectedRule != null)
            {
                SelectedRule.SetField(new List<RuleField>(RuleFields));
            }
        }

        public void EditChapter(string data, string data1)
        {
            SelectedChapter.Caption = data;
            if (SelectedChapter is Chapter)
            {
                (SelectedChapter as Chapter).ChapterTitle = new ChapterTitleTemplate()
                {
                    StartWith = data1
                };
            }
            var c = new ObservableCollection<DocPart>(_chapters);
            Chapters = c;
        }
    }
}
