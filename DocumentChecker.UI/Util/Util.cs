﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DocumentChecker.UI.Util
{
    class Utilities
    {
        public static Window GetWindowRef(string windowName)
        {
            Window retVal = null;
            foreach (Window window in Application.Current.Windows)
            {
                // The window's Name is set in XAML.
                if (window.Name.Trim().ToLower() == windowName.Trim().ToLower())
                {
                    retVal = window;
                    break;
                }
            }

            return retVal;
        }
    }
}
