﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using DocumentChecker.Model;
using DocumentChecker.Model.Annotations;
using DocumentChecker.Model.Parts;

namespace DocumentChecker.UI
{
    /// <summary>
    /// Interaction logic for AdditionalWindow.xaml
    /// </summary>
    public partial class AdditionalWindow : Window, INotifyPropertyChanged
    {
        private List<string> _rulesItems;
        public List<string> RulesItems
        {
            get { return _rulesItems; }
            set
            {
                _rulesItems = value;
                OnPropertyChanged("RulesItems");
            }
        }

        private string _selectedRule;
        public string SelectedRule
        {
            get { return _selectedRule; }
            set
            {
                _selectedRule = value;
                OnPropertyChanged("SelectedRule");
            }
        }

        private string _enterData;
        public string EnterData
        {
            get { return _enterData; }
            set
            {
                _enterData = value;
                OnPropertyChanged("EnterData");
            }
        }

        private string _data;
        public string Data
        {
            get { return _data; }
            set
            {
                _data = value;
                OnPropertyChanged("Data");
            }
        }

        private string _enterData1;
        public string EnterData1
        {
            get { return _enterData1; }
            set
            {
                _enterData1 = value;
                OnPropertyChanged("EnterData1");
            }
        }

        private string _data1;
        public string Data1
        {
            get { return _data1; }
            set
            {
                _data1 = value;
                OnPropertyChanged("Data1");
            }
        }

        private Visibility _firstRowVis;
        public Visibility FirstRowVis
        {
            get { return _firstRowVis; }
            set
            {
                _firstRowVis = value;
                OnPropertyChanged("FirstRowVis");
            }
        }

        private Visibility _secondRowVis;
        public Visibility SecondRowVis
        {
            get { return _secondRowVis; }
            set
            {
                _secondRowVis = value;
                OnPropertyChanged("SecondRowVis");
            }
        }
       
        private Visibility _comboBoxVis;
        public Visibility ComboBoxVis
        {
            get { return _comboBoxVis; }
            set
            {
                _comboBoxVis = value;
                OnPropertyChanged("ComboBoxVis");
            }
        }

        private DocTypesWindow mainWindow;
        private string type;

        public AdditionalWindow(List<string> enterData, DocTypesWindow window)
        {
            type = "rule";
            ComboBoxVis = Visibility.Visible;
            FirstRowVis = Visibility.Collapsed;
            SecondRowVis = Visibility.Collapsed;

            RulesItems = enterData;

            InitializeComponent();
            DataContext = this;

            mainWindow = window;
        }

        public AdditionalWindow(string enterData, string enterData1, DocTypesWindow window)
        {
            type = "chapter";
            ComboBoxVis = Visibility.Collapsed;
            FirstRowVis = Visibility.Visible;
            SecondRowVis = Visibility.Visible;

            EnterData = enterData;
            Data = "";

            EnterData1 = enterData1;
            Data1 = "";

            InitializeComponent();
            DataContext = this;
            mainWindow = window;
        }

        private bool editing = false;
        public AdditionalWindow(string enterData, string enterData1, DocPart docPart, DocTypesWindow window)
        {
            editing = true;
            type = "chapter";
            ComboBoxVis = Visibility.Collapsed;
            FirstRowVis = Visibility.Visible;
            SecondRowVis = Visibility.Visible;

            EnterData = enterData;
            Data = docPart.Caption;

            EnterData1 = enterData1;
            Data1 = (docPart is Chapter)? (docPart as Chapter).ChapterTitle.StartWith : "";

            InitializeComponent();
            DataContext = this;
            mainWindow = window;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void AcceptButtonClick(object sender, RoutedEventArgs e)
        {
            if (type == "chapter")
            {
                if(!editing)
                    mainWindow.AddingChapter(Data, Data1);
                else
                    mainWindow.EditChapter(Data, Data1);
            }else if (type == "rule")
            {
                mainWindow.AddingRule(SelectedRule);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
