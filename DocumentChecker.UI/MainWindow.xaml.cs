﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;
using System.IO;
using DocumentChecker.Model;
using DocumentChecker.Model.Docs;
using DocumentChecker.UI.ViewModel;


namespace DocumentChecker.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = viewModel;
            viewModel.IsTextButEnable = false;
           // Reload();
        }
        MainWindowViewModel viewModel = new MainWindowViewModel();

        #region DragnDropEvent
        private void FileShowTextBox_PreviewDragEnter(object sender, DragEventArgs e)
        {
            bool isCorrect = true;
            if (e.Data.GetDataPresent(DataFormats.FileDrop, true))
            {
               // MessageBox.Show(e.Source.ToString());
                string[] filenames = (string[])e.Data.GetData(DataFormats.FileDrop, true);
                foreach (string filename in filenames)
                {
                    if (File.Exists(filename) == false)
                    {
                        isCorrect = false;
                        break;
                    }
                    FileInfo info = new FileInfo(filename);
                    if (info.Extension != ".doc" && info.Extension != ".docx")
                    {
                        isCorrect = false;
                        break;
                    }
                }
            }
            e.Effects = isCorrect ? DragDropEffects.All : DragDropEffects.None;
            e.Handled = true; 
        }

        private void FileShowTextBox_PreviewDrop(object sender, DragEventArgs e)
        {
            
            var filenames = (string[])e.Data.GetData(DataFormats.FileDrop, true);
            FileShowTextBox.Text = filenames[0];
            var sss = (Storyboard)TryFindResource("FUEPathText");
            sss.Stop();
            var s = (Storyboard)TryFindResource("FUEStoryboard");
            s.Stop();
            viewModel.FUEOb = "SearchBut";
            s.Begin();
            SearchBut.IsEnabled = true;
            if (e.Data.GetDataPresent("FileName"))
            {
                e.Data.GetData("FileName");
                e.Data.GetData(typeof(Object));
            }
            e.Handled = true; 
        }

        private void Win_PreviewDragEnter(object sender, DragEventArgs e)
        {
            viewModel.PathText = "Перенесіть сюди файл";
            e.Effects = DragDropEffects.All;
            e.Handled = true;
        }
#endregion
        #region StoryboardFunc
        private void Win_Loaded(object sender, RoutedEventArgs e)
        {
            if (Properties.Settings.Default.IsFUE != true) return;
            /* Storyboard s = (Storyboard)TryFindResource("FUEStoryboard");
                s.Begin();
                viewModel.FUEOb = "SearchBut";*/
            /*var s = (Storyboard) TryFindResource("FUETypeText");
            s.Begin();*/
            var sss = (Storyboard)TryFindResource("FUEPathText");
            sss.Begin();
            var ss = (Storyboard)TryFindResource("FUEStoryboard");
            sss.Begin();
            ss.Begin();
            viewModel.FUEOb = "SearchBut";
            viewModel.Reload();
        }

        private void OpenBut_Click(object sender, RoutedEventArgs e)
        {
            if (Properties.Settings.Default.IsFUE)
            {
                var sss = (Storyboard)TryFindResource("FUEPathText");
                sss.Stop();
                var s = (Storyboard) TryFindResource("FUEStoryboard");
                s.Stop();
                s.Begin();
            }
        }

        private void SearchBut_Click(object sender, RoutedEventArgs e)
        {
            if (Properties.Settings.Default.IsFUE)
            {
                var s = (Storyboard) TryFindResource("FUEStoryboard");
                s.Stop();
                viewModel.FUEOb = "SendBut";
                s.Begin();
            }
        }

        private void SendBut_Click(object sender, RoutedEventArgs e)
        {
            if (Properties.Settings.Default.IsFUE)
            {
                var s = (Storyboard) TryFindResource("FUEStoryboard");
                s.Stop();
            }
        }

        private int _count;
        private void FileTypeCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_count < 2)
            {
                _count++;
            }
            else
            {
                if (Properties.Settings.Default.IsFUE)
                {
                    var ss = (Storyboard) TryFindResource("FUETypeText");
                    ss.Stop();
                    
                    viewModel.FUEOb = "SearchBut";
               }
                viewModel.IsOpenButEnable = true;
                viewModel.IsTextButEnable = true;
            }
        }
#endregion

        private void Image_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            MessageBox.Show(" Ця програма перевіряє лише форматування і не перевіряє стилістики, формул, математичних моделей, так як не володіє штучним інтелектом.");
        }


        private void LoadDocTypeButton_OnClick(object sender, RoutedEventArgs e)
        {
            /*
            var d = new TheDocumentContent();
            d.LoadRules(@"G:\NULP\8sem\diploma\diploma.xml");
            
            var dL = new DiplomaLight();
            dL.LoadRules(@"G:\NULP\8sem\diploma\diplomaLight.xml");*/
            var win = new DocTypesWindow();
            win.Show();
        }

        private void ReloadButton_OnClick(object sender, RoutedEventArgs e)
        {
            viewModel.Reload();
        }
    }
}
