﻿using System.Windows;
using DocumentChecker.UI.ViewModel;

namespace DocumentChecker.UI
{
    /// <summary>
    /// Логика взаимодействия для PropertyWindow.xaml
    /// </summary>
    public partial class PropertyWindow
    {
        public PropertyWindow()
        {
            InitializeComponent();
            DataContext = new MainWindowViewModel();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
           Properties.Settings.Default.Reload();
            if (Chekbox.IsChecked != null)
                Properties.Settings.Default.IsSplashEnable = (bool)Chekbox.IsChecked;
            if (FueChekbox.IsChecked != null) Properties.Settings.Default.IsFUE = (bool) FueChekbox.IsChecked;

            Properties.Settings.Default.FirstName = FirstName.Text;
            Properties.Settings.Default.LastName = LastName.Text;
            Properties.Settings.Default.Save();
            Close();
        }
    }
}
