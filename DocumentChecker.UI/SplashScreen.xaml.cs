﻿using System;
using System.Windows;
using DocumentChecker.UI.Properties;
using DocumentChecker.UI.ViewModel;

namespace DocumentChecker.UI
{
    /// <summary>
    /// Логика взаимодействия для SplashScreen.xaml
    /// </summary>
    public partial class SplashScreen
    {
        MainWindowViewModel m=new MainWindowViewModel();
        public SplashScreen()
        {
            InitializeComponent();
            DataContext = m;
            if (Settings.Default.IsSplashEnable)
            {
                var win = new MainWindow();
                win.Show();
                Close();
            }
        }

       

        private void Window_Closed_1(object sender, EventArgs e)
        {
            if (Chekbox.IsChecked != null) Settings.Default.IsSplashEnable = (bool)Chekbox.IsChecked;
            Settings.Default.Save();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            var win = new MainWindow();
            win.Show();
            Close();
        }

        

       
    }
}
